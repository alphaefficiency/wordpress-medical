{{--
  Template Name: Book Online Template
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
  <div class="container-fluid book-header">
    <h1>Book Online Now</h1>
    <div class="book-mobile">
      <h6>Start your journey today!<br/><strong>- Nurse Walton</strong></h6>
    </div>
  </div>
  <div class="container-fluid contact-main">
    <div class="contact-wrap">
      {{-- <!-- Calendly inline widget begin -->
      <div class="calendly-inline-widget" data-url="https://calendly.com/betterweighmedical/15min?background_color=fff" style="min-width:320px;height:630px;"></div>
      <script type="text/javascript" src="https://assets.calendly.com/assets/external/widget.js"></script>
      <!-- Calendly inline widget end --> --}}

      <iframe src="https://app.acuityscheduling.com/schedule.php?owner=21985813" title="Schedule Appointment" width="100%" height="800" frameBorder="0"></iframe><script src="https://embed.acuityscheduling.com/js/embed.js" type="text/javascript"></script>
      {{-- <div class="decor decor-l">
        <svg width="221" height="221" viewBox="0 0 221 221" fill="none" xmlns="http://www.w3.org/2000/svg">
          <circle cx="110.5" cy="110.5" r="96.5" stroke="white" stroke-width="28"/>
        </svg>          
      </div>
      <div class="decor decor-r">
        <svg width="327" height="327" viewBox="0 0 327 327" fill="none" xmlns="http://www.w3.org/2000/svg">
          <circle cx="163.5" cy="163.5" r="149.5" stroke="white" stroke-width="28"/>
        </svg>          
      </div> --}}
    </div>
  </div>
    @include('components.testimonials')
  @endwhile
@endsection
