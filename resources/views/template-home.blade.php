{{--
  Template Name: Home Template
--}}

@extends('layouts.app')

@section('content')
  @include('partials.home.promotions-slider')
  {{-- @include('partials.home.hero') --}}
  @include('partials.home.second-sec')
  @include('partials.home.explore-program')
  @include('partials.home.success-stories')
  @include('partials.home.how-it-works')
  @include('partials.home.why-choose-us')
  @include('partials.home.shop-exclusive')
  @include('partials.home.current-specials')
@endsection
