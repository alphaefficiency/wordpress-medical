{{--
  Template Name: Locations Template
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.location.header')
    {{-- @php
      echo do_shortcode('[wpgmza id="1"]');
    @endphp --}}
    @include('partials.location.map')
    @include('partials.location.new-you')
  @endwhile
@endsection
