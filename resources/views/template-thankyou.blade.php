{{--
  Template Name: ThankYou
--}}


@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
  @include('partials.thankyou.thankyou-section')
  @endwhile
@endsection