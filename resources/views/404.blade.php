@extends('layouts.app')

@section('content')


    <div class="content404">
        <h1>
            404
        </h1>
        <p>We are sorry, but the page you requested was not found.</p>
        <a class="color-btn" href="{{ get_site_url() }}">Go Back</a>

        <svg class="bubble1" width="197" height="197" viewBox="0 0 197 197" fill="none" xmlns="http://www.w3.org/2000/svg">
            <circle cx="98.5" cy="98.5" r="84.5" stroke="white" stroke-width="28" />
        </svg>

        <svg class="bubble2" width="367" height="367" viewBox="0 0 367 367" fill="none" xmlns="http://www.w3.org/2000/svg">
            <circle cx="183.5" cy="183.5" r="169.5" stroke="white" stroke-width="28" />
        </svg>

        <div class="image404">
            <img src="@asset('images/404/excited-woman.png')" alt="">
        </div>
    </div>



@endsection
