<div class="container-fluid testimonials">
  <div class="container">
    <div class="testimonial-text">
      <h2 class="title">Testimonials</h2>
      <p>See What Other’s Are Saying About Us, & Find Inspiration And Motivation To Join Them On The Path To A Better Weigh.</p>
      <a href="https://www.yelp.com/writeareview/biz/GRbxEpVtMYkUsjrSfC1LaA?return_url=%2Fbiz%2FGRbxEpVtMYkUsjrSfC1LaA&source=biz_details_war_button" target="blank" class="color-btn">Submit a Review</a>
    </div>
    <div id="testimonialCarousel" class="carousel slide" data-ride="carousel">
      <a class="carousel-control-prev" href="#testimonialCarousel" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <div class="carousel-inner">
        <div class="carousel-item active">
          <div class="testimonial-header">
            <img src="@asset('images/common/testimonials/paris.png')" alt="Paris Stallworth"/>
            <div class="name-info">
              <p class="name">Paris Stallworth</p>
              {{-- <p class="lost-amount">Lost 36 pounds in 3 months</p> --}}
            </div>
          </div>
          <p>“Great products friendly staff! And the products really work! Lost 30lbs in 3 months on their products..and that is with little to no exercise! Would probably have been more if I did exercise. Next round I will exercise for better results”</p>
        </div>
        <div class="carousel-item">
          <div class="testimonial-header">
            <img src="@asset('images/common/testimonials/Kenyatta.png')" alt="Kenyatta Smith"/>
            <div class="name-info">
              <p class="name">Kenyatta Smith</p>
              {{-- <p class="lost-amount">Lost 36 pounds in 3 months</p> --}}
            </div>
          </div>
          <p>“I am very satisfied with A BETTER WEIGH.
            The staff is very nice, respectable, knowledgeable. They are there to help you lose and keep the weight off. I feel really good and energized after leaving A BETTER WEIGH!!”</p>
        </div>
        <div class="carousel-item">
          <div class="testimonial-header">
            <img src="@asset('images/common/testimonials/Brian.png')" alt="Brian McCoy"/>
            <div class="name-info">
              <p class="name">Brian McCoy</p>
              {{-- <p class="lost-amount">Lost 36 pounds in 3 months</p> --}}
            </div>
          </div>
          <p>“I went back to Better weight this year 2019 in May and I was a 40 waist I'm now 36  Lipo-Ignite Lipotropic Injection  Does the job without working out just eating healthy I Recommend it Great”</p>
        </div>
        <div class="carousel-item">
          <div class="testimonial-header">
            <img src="@asset('images/common/testimonials/Gwendolyn.png')" alt="Gwendolyn Czupryn"/>
            <div class="name-info">
              <p class="name">Gwendolyn Czupryn</p>
              {{-- <p class="lost-amount">Lost 36 pounds in 3 months</p> --}}
            </div>
          </div>
          <p>“Ashley the manager is always courteous and very very respectful and always helps me when I'm there
            I love the doctor as well she is very informative and her personality is wonderful. It's a great place”</p>
        </div>
        <div class="carousel-item">
          <div class="testimonial-header">
            <img src="@asset('images/common/testimonials/katrice.jpg')" alt="Katrice Donson"/>
            <div class="name-info">
              <p class="name">Katrice Donson</p>
              {{-- <p class="lost-amount">Lost 36 pounds in 3 months</p> --}}
            </div>
          </div>
          <p>“Awesome service and for my first time there on a Saturday ..it was busy had a lot of new patients but the staff still greeted everyone who walked in the door nothing but smiles that made my day.. I like how the staff moves & works together getting everyone out on this busy Saturday & so they can continue on with their day..”</p>
        </div>
      </div>
      <a class="carousel-control-next" href="#testimonialCarousel" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
    <div class="decor decor-l">
      <svg width="394" height="394" viewBox="0 0 394 394" fill="none" xmlns="http://www.w3.org/2000/svg">
        <circle opacity="0.15" cx="197" cy="197" r="183" stroke="white" stroke-width="28"/>
      </svg>        
    </div>
    <div class="decor decor-r">
      <svg width="203" height="203" viewBox="0 0 203 203" fill="none" xmlns="http://www.w3.org/2000/svg">
        <circle opacity="0.15" cx="101.5" cy="101.5" r="87.5" stroke="white" stroke-width="28"/>
      </svg>        
    </div>
  </div>
</div>