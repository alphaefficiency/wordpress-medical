{{-- Single post article on Blog page (not Single Page Post) --}}

<article class="single-post">
  <div class="post-featured-image">
    <div class="post-image-link">
      @if (has_post_thumbnail($post->ID))
        <?=get_the_post_thumbnail($post->ID, 'full')?>
      @else
        <img class="post-thumbnail" src="@asset('images/blog/default.png')" alt="Post image"/>
      @endif
    </div>
  </div>
  <div class="post-text">
    <p class="date"><?= get_the_date('F j, Y', $post->ID)?></p>
    <h2>
        {{ Posts::getExcerptForPostTitle($post->post_title, 230) }}
    </h2>
    <div class="post-author">
      <img src="@asset('images/blog/michelle.png')" alt="Author"/>
      <p>Nurse Walton</p>
    </div>
    <p class="post-date"><?= get_the_date('F j. Y', $post->ID)?></p>
    @if(get_post_meta($post->ID, 'mediaLink', true))
      <a href="{{ get_post_meta($post->ID, 'mediaLink', true) }}" target="_blank" rel="noopener noreferrer" class="color-btn press-read-more-btn">Read More</a>
    @endif
  </div>
</article>