{{-- Single post article on Blog page (not Single Page Post) --}}

<?php
  $tags = get_the_tags();
?>

<article class="single-post">
  <div class="post-featured-image">
    <a class="post-image-link" href="{{ get_permalink($post->ID) }}">
      @if (has_post_thumbnail($post->ID))
        <?=get_the_post_thumbnail($post->ID, 'full')?>
      @else
        <img class="post-thumbnail" src="@asset('images/blog/default.png')" alt="Post image"/>
      @endif
    </a>
  </div>
  <a class="post-text" href="{{ $post->link }}">
    <p class="date"><?= get_the_date('F j, Y', $post->ID)?></p>
    <h2>
        {{ Posts::getExcerptForPostTitle($post->post_title, 230) }}
    </h2>
    <p class="post-description">
      @php echo Posts::getExcerptForPostContent($post->post_content) @endphp
    </p>
    <div class="post-author">
      <img src="@asset('images/blog/michelle.png')" alt="Author"/>
      <p>Nurse Walton</p>
    </div>
  </a>
</article>