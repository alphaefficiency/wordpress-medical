{{--
  Template Name: Franchise Template
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.franchise.franchise-hero')
    @include('partials.franchise.real-opportunities')
    @include('partials.franchise.support-benefits')
    @include('partials.franchise.join-costs')
    @include('partials.franchise.franchise-form')
  @endwhile
@endsection
