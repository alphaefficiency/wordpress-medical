{{--
  Template Name: Press Template
--}}

@extends('layouts.app')

@section('content')
  @if(have_posts())
    @while(have_posts()) @php the_post() @endphp
    <div class="container-fluid blog-wrapper">
      <div class="gray"></div>
      @php
        // Get Posts by page
        $postsPerPage = intval(get_option('posts_per_page'));
        // Total number of pages
        $totalPages = Posts::mediaCustomPostTypePagination(9);

        $res = Posts::getCurrentPage($_GET['pageNumber'], $totalPages);
        $currentPage = $res->currentPage;
        $is404 = $res->is404;

        // Get posts per page
        $posts = Posts::getMediaCustomPostType(9, $currentPage);
      @endphp

      {{-- If user requested page that doesn't exist return 404 --}}
      @if ($is404)
        @include('404')
        @php return; @endphp
      @endif
       
      {{-- Display Posts --}}
      <div class="container blog-main">
        <h1 class="title">Media Press</h1>
        <ul class="post-list">
          @foreach($posts as $post)
            @component('components.posts.press-post' , ['post' => $post]) @endcomponent
          @endforeach
        </ul>
      </div>

    @endwhile
  </div>

    {{-- Blog Pagination --}}
    @component('components.blog.press-pagination' , ['currentPage' => $currentPage , 'counted' => $totalPages]) @endcomponent
    

    {{-- If there are no posts, display this message --}}
    @else
      <p>@php _e('Sorry, no posts matched your criteria.'); @endphp</p>
  @endif

@endsection

