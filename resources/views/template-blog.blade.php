{{--
  Template Name: Blog
--}}

@extends('layouts.app')

@section('content')
  @if(have_posts())
    @while(have_posts()) @php the_post() @endphp
    <div class="container-fluid blog-wrapper">
      <div class="gray"></div>
      @php
        // Get Posts by page
        $postsPerPage = intval(get_option('posts_per_page'));
        // Total number of pages
        $totalPages = Posts::pagination(3);

        $res = Posts::getCurrentPage($_GET['pageNumber'], $totalPages);
        $currentPage = $res->currentPage;
        $is404 = $res->is404;

        // Get posts per page
        $posts = Posts::getPostsByPage(5, $currentPage);
      @endphp

      {{-- If user requested page that doesn't exist return 404 --}}
      @if ($is404)
        @include('404')
        @php return; @endphp
      @endif
       
      {{-- Display Posts --}}
      <div class="container blog-main">
        <h1 style="display: none;">Blog</h1>
        @component('components.posts.post-list' , ['posts' => $posts]) @endcomponent
        @component('components.blog-aside', ['posts' => $posts]) @endcomponent
      </div>

    @endwhile
  </div>

    {{-- Blog Pagination --}}
    @component('components.blog.blog-pagination' , ['currentPage' => $currentPage , 'counted' => $totalPages]) @endcomponent
    

    {{-- If there are no posts, display this message --}}
    @else
      <p>@php _e('Sorry, no posts matched your criteria.'); @endphp</p>
  @endif

@endsection

