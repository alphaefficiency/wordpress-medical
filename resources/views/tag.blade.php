{{--
  Template Name: Blog
--}}

@extends('layouts.app')

@section('content')
  @if(have_posts())
    <div class="container-fluid blog-wrapper">
      <div class="gray"></div>
      @php
        // Get category from slug
        $pageTagId = get_queried_object()->term_id;
        $allTags = get_the_tags();
        foreach($allTags as $pageTag) {
          if($pageTag->term_id == $pageTagId) {
            $tagId = $pageTag->term_id;
            $slug = $pageTag->slug;
            $postCounted = $pageTag->count;
          }
        }

        // Get Posts by page
        $postsPerPage = intval(get_option('posts_per_page'));
        // Total number of pages
        $totalPages = Posts::tagPagination(5, $tagId);

        $res = Posts::getCurrentPage($_GET['pageNumber'], $totalPages);
        $currentPage = $res->currentPage;
        $is404 = $res->is404;

        // Get posts per page
        $posts = Posts::getPostsByTag(5, $currentPage, $tagId);
      @endphp

      {{-- If user requested page that doesn't exist return 404 --}}
      @if ($is404)
        @include('404')
        @php return; @endphp
      @endif
       
      {{-- Display Posts --}}
      <div class="container blog-main">
        <h1 style="display: none;">Blog</h1>
        @component('components.posts.post-list' , ['posts' => $posts]) @endcomponent
        @component('components.blog-aside', ['posts' => $posts]) @endcomponent
      </div>

  </div>

    {{-- Blog Pagination --}}
    <div class="container">
      @component('components.blog.tag-pagination' , ['currentPage' => $currentPage , 'counted' => $totalPages, 'slug' => $slug]) @endcomponent
    </div>
    

    {{-- If there are no posts, display this message --}}
    @else
      <p>@php _e('Sorry, no posts matched your criteria.'); @endphp</p>
  @endif

@endsection

