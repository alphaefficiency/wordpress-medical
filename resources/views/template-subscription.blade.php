{{--
  Template Name: Subscription Template
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.subscription.header')
    @include('partials.subscription.what-is-lipo-ignite')
    @include('partials.subscription.nutrients-include')
    @include('partials.subscription.customer-reviews')
    @include('partials.subscription.covid')
  @endwhile
@endsection
