{{--
  Template Name: About Template
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.about.header')
    @include('partials.about.second')
    @include('partials.about.story')
    @include('partials.about.your-body')
    @include('components.testimonials')
  @endwhile
@endsection
