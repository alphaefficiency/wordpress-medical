<div class="container-fluid join-costs-holder">
  <div class="container join-costs-container">
    <div class="join-holder">
      <div class="join-content">
        <h2 class="join-title">
          Why Should You <br> Join A Better Weigh?
        </h2>
        <p class="join-text">
          There has never been a better day to join A Better Weigh! Why? Because the billion dollar weight loss
          market is stable and projected to grow to 2.6% annually through
          2023 . We are giving you the opportunity to join a company that has been an industry leader in our
          local market for 10+ years. A proven program and highly rated products draw our customers in, keep
          them coming back, and produce referrals!
        </p>
      </div>
      <div class="all-team">
        <img src="@asset("images/franchise/all-team.png")" alt="medicine-team" class="all-team-img">
      </div>
    </div>
    <div class="costs-holder">
      <div class="checking-weight">
        <img src="@asset("images/franchise/checking-weight.png")" alt="girl checking weight" class="checking-weight-img">
      </div>
      <div class="costs-content">
        <h2 class="costs-title">
          What Are the Costs?
        </h2>
        <p class="costs-text">
            Franchise Fee: $50,000 <br>
            Royalty Fee: 6% of gross sales <br>
            Marketing Fund: 1% of gross sales <br>
            The total investment necessary to <br class="begin"> begin operation of a franchise is <br class="is"> between $108,400 to $235,250 <br class="which">
            That Investment includes <br class="includes"> $20,000 to $40,000 working capital <br class="capital"> for various expenses during <br class="during"> the first three months 
            of operation. <br>
            Liquid Assets: $50,000 <br>
            Net Worth: $170,000 
        </p>
      </div>
    </div>
  </div>
</div>
