<div class="container-fluid support-benefits-holder">
  <div class="container support-benefits-container">
    <div class="support-benefits">
      <div class="support-content">
        <h2 class="support-title">
          Better Support.<br> Better Marketing.
        </h2>
        <p class="support-text">
          When you start your franchise with A Better Weigh, you are gaining a team of experts that has your back
          every step of the way! As a franchisee, you gain...
          <br><span class="dot"></span> Marketing Guidance
          <br><span class="dot"></span> Ongoing Operational Support
          <br><span class="dot"></span> Dual-Phase Training Program
          <br><span class="dot"></span> Exclusive Territory
        </p>
      </div>
      <div class="benefits-content">
        <h2 class="benefits-title">
          More Benefits of <br> A Better Weigh
        </h2>
        <p class="benefits-text">
          As a A Better Weigh franchise partner, you are a part of a brand that dialed in every process and product to
          be the best
          of the best. You has already dialed in several perks with A Better Weigh:
          <br><span class="dot"></span> Access to industry-leading products, meal plans and programs
          <br><span class="dot"></span> Custom branded software
          <br><span class="dot"></span> Strong, vibrant branding
          <br><span class="dot"></span> Recurring revenue opportunities
        </p>
      </div>
    </div>
  </div>
</div>
