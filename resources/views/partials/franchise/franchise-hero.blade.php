<div class="container-fluid hero-holder">
  <div class="container franch-hero-container">
      <div class="hero-holder-small">
    <div class="franch-hero-content">
      <h2 class="franch-title">
        The Best Way To A Great Future: A Better Weigh
        Franchise Opportunity!
      </h2>
     <p class="hero-text">
      There’s a better way to higher profits, a feel-good business, and growth that counts! Get ready for a
      proven opportunity in franchising: A Better Weigh Medical Weight Loss Centers!
      
      Come and join A Better Weigh to make a piece of the $7.27 billion dollar weight loss market through our franchise system.
       We hand you the keys to success through ongoing support, launch assistance, in-depth training and more.
        Launch a new future with A Better Weigh!
     </p>
    </div>
    <div class="hero-girls">
        <img src="@asset('images/franchise/hero-girls.png')" alt="girls that lost weight" class="hero-girls-img">
        <img src="@asset('images/franchise/hero-girls-mob.png')" alt="girls that lost weight" class="hero-girls-img-mob">
    </div>
    </div>
  </div>
</div>
