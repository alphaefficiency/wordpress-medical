<div class="container-fluid opportunity-holder">
  <div class="container opportunity-container">
    <div class="opportunity-holder-small">
      <div class="opp-left">
        <img src="@asset('images/franchise/opp-girls.png')" alt="" class="opp-girls">
      </div>
      <div class="opp-right">
        <h2 class="opportunity-title">
          Real Opportunities. <br>Real Results.
        </h2>
        <p class="opportunity-text">
          Over a decade, Our leadership team at A Better Weigh operates to operate
          on a different level than other weight loss avenues. Combining extensive research, industry know-how, and a passion for helping others, we’ve strategically
          designed a franchise system that will deliver results into your hands. A Better
          Weigh’s friendly atmosphere, excellent customer care. And proprietary products and programs keep
          customers coming back.
          Our customers choose from four FDA-approved programs for our clients to
          choose from — Basic, Excellerated, Elite and CraveX — all physician supervised.
          Turnkey franchise models are available, fully stocked and staffed for only $125K!
        </p>
      </div>

    </div>
  </div>
</div>
