<div class="container-fluid franchise-form-holder">
  <div class="container franchise-form-container">
    <div class="join-form-holder">
      <div class="join-left">
        <h2 class="join-form-title">
          WANT TO FRANCHISE?
        </h2>
        <h6 class="join-form-sub">
          Looking to join A Better Weigh?
        </h6>
        <p class="join-form-text">
          Let us know if you're seeking franchise opportunity and fill out the form and one of our staff members will
          contact you.
        </p>
        <img src="@asset("images/franchise/good-looking.png")" alt="good looking girl" class="good-looking">
      </div>
      <div class="join-right">
        <?=do_shortcode('[contact-form-7 id="8907" title="Contact form 1"]')?>
        <script>
          document.addEventListener( 'wpcf7mailsent', function( event ) {
          location = 'https://betterweighmedical.com/thank-you-2/';
          }, false );
          </script>
      </div>
    </div>
  </div>
  <img src="@asset("images/franchise/good-looking-mob.png")" alt="good looking girl" class="good-looking-mob">
</div>
