<section class="thankyou-section">
  <div class="hero">

    <div class="womans">
      <img src="@asset('images/thankyou/womans.png')" alt="womans" class="womans-desktop">
      <img src="@asset('images/thankyou/mobile.png')" alt="womans" class="womans-mobile">
    </div>

    <div class="text">
      <h1>Thank you!</h1>
      <p>We hand
        you the keys to success through ongoing support, launch assistance, in-depth training and more.
        Launch a new future with A Better Weigh!
      </p>
      {{-- <input type="button" value="Download PDF" class="download"> --}}
      <a href="https://betterweighmedical.com/wp-content/uploads/2021/12/A-Better-Weigh-Brochure-v1.pdf" class="download">Download PDF</a>
    </div>
  </div>

</section>
