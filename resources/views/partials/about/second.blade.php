<div class="container-fluid second">
  <div class="container">
    <div class="item">
      <div class="item-header">
        <svg width="45" height="45" viewBox="0 0 45 45" fill="none" xmlns="http://www.w3.org/2000/svg">
          <circle cx="22.5" cy="22.5" r="22.5" fill="url(#paint0_linear)"/>
          <path fill-rule="evenodd" clip-rule="evenodd" d="M13.1565 22.4863C15.7135 19.0906 19.1788 17 23 17C26.8212 17 30.2865 19.0906 32.8435 22.4904C33.0522 22.7686 33.0522 23.1573 32.8435 23.4355C30.2865 26.8312 26.8212 28.9219 23 28.9219C19.1788 28.9219 15.7135 26.8312 13.1565 23.4314C12.9478 23.1532 12.9478 22.7645 13.1565 22.4863ZM23.1391 26.1245C24.8189 26.1245 26.1806 24.7627 26.1806 23.083C26.1806 21.4032 24.8189 20.0415 23.1391 20.0415C21.4594 20.0415 20.0977 21.4032 20.0977 23.083C20.0977 24.7627 21.4594 26.1245 23.1391 26.1245Z" fill="white"/>
          <defs>
          <linearGradient id="paint0_linear" x1="0" y1="0" x2="45" y2="45" gradientUnits="userSpaceOnUse">
          <stop stop-color="#FFD900"/>
          <stop offset="1" stop-color="#DD71C6"/>
          </linearGradient>
          </defs>
        </svg>
        <h5>Physician-Supervised</h5>      
      </div>
      <p>Each of our weight loss programs is overseen by a physician who takes into account your medical history and ensures that you receive the highest level of care.</p>
    </div>
    <div class="item">
      <div class="item-header">
        <svg width="45" height="45" viewBox="0 0 45 45" fill="none" xmlns="http://www.w3.org/2000/svg">
          <circle cx="22.5" cy="22.5" r="22.5" fill="url(#paint0_linear)"/>
          <path d="M14.3854 31.6146H16.3646C17.1286 31.6146 17.75 30.9932 17.75 30.2292V21.125C17.75 20.361 17.1286 19.7396 16.3646 19.7396H14.3854C13.6215 19.7396 13 20.361 13 21.125V30.2292C13 30.9932 13.6215 31.6146 14.3854 31.6146Z" fill="white"/>
          <path d="M23.1183 14C22.3266 14 21.9308 14.3958 21.9308 16.375C21.9308 18.256 20.1092 19.7697 18.9375 20.5495V30.3567C20.205 30.9433 22.7423 31.8126 26.6808 31.8126H27.9475C29.4913 31.8126 30.8054 30.7042 31.0667 29.1842L31.9534 24.0384C32.2859 22.0988 30.7975 20.3334 28.8342 20.3334H25.0975C25.0975 20.3334 25.6912 19.1459 25.6912 17.1667C25.6912 14.7917 23.91 14 23.1183 14Z" fill="white"/>
          <defs>
          <linearGradient id="paint0_linear" x1="0" y1="0" x2="45" y2="45" gradientUnits="userSpaceOnUse">
          <stop stop-color="#FFD900"/>
          <stop offset="1" stop-color="#DD71C6"/>
          </linearGradient>
          </defs>
        </svg>          
        <h5>Simple & Affordable</h5>      
      </div>
      <p>We believe that losing weight the smarter way should be accessible to everyone. Our practice provides experienced care at a fraction of the cost of most other weight loss clinics.</p>
    </div>
    <div class="item">
      <div class="item-header">
        <svg width="45" height="45" viewBox="0 0 45 45" fill="none" xmlns="http://www.w3.org/2000/svg">
          <circle cx="22.5" cy="22.5" r="22.5" fill="url(#paint0_linear)"/>
          <path d="M31.7554 15.7991C31.7192 15.5412 31.5322 15.3291 31.2794 15.2594L23.1766 13.0239C23.0611 12.992 22.939 12.992 22.8234 13.0239L14.7206 15.2594C14.4678 15.3291 14.2808 15.5411 14.2446 15.7991C14.1976 16.1344 13.1255 24.0556 15.8754 27.9962C18.6222 31.9321 22.6742 32.9407 22.8453 32.9818C22.8962 32.994 22.948 33 23 33C23.052 33 23.1038 32.9939 23.1547 32.9818C23.3259 32.9407 27.3779 31.9321 30.1246 27.9962C32.8745 24.0557 31.8024 16.1344 31.7554 15.7991ZM28.2253 20.4236L22.6983 25.9066C22.5697 26.0342 22.4011 26.0981 22.2325 26.0981C22.0639 26.0981 21.8953 26.0343 21.7667 25.9066L18.3494 22.5165C18.2258 22.394 18.1564 22.2277 18.1564 22.0544C18.1564 21.8811 18.2259 21.7148 18.3494 21.5923L19.0279 20.9191C19.2852 20.664 19.7024 20.6639 19.9596 20.9191L22.2325 23.174L26.6151 18.8261C26.7386 18.7035 26.9062 18.6347 27.0809 18.6347C27.2556 18.6347 27.4232 18.7035 27.5468 18.8261L28.2253 19.4993C28.4826 19.7545 28.4826 20.1684 28.2253 20.4236Z" fill="white"/>
          <defs>
          <linearGradient id="paint0_linear" x1="0" y1="0" x2="45" y2="45" gradientUnits="userSpaceOnUse">
          <stop stop-color="#FFD900"/>
          <stop offset="1" stop-color="#DD71C6"/>
          </linearGradient>
          </defs>
        </svg>          
        <h5>Safe & Effective</h5>      
      </div>
      <p style="padding: 0;">Your safety as a patient is our top priority. All patients undergo a thorough medical evaluation and are asked to provide a list of current medications to make sure you receive the right treatment.</p>
    </div>
  </div>
  <div class="decor">
    <svg width="238" height="238" viewBox="0 0 238 238" fill="none" xmlns="http://www.w3.org/2000/svg">
      <circle cx="119" cy="119" r="105" stroke="#F9F9F9" stroke-width="28"/>
    </svg>    
  </div>
</div>