<div class="container-fluid our-story">
  <div class="container">
    <h2 class="title" style="display: none;">Our Story</h2>
    <a href="#" class="color-btn mobile-color-btn" style="display: none;">Talk With Our Team</a>
    <div class="story-text">
      <div class="first-tab">
        <h2 class="title">Our Story</h2>
        <p>A Better Weigh Weight Loss Centers were started by Nurse Chanay Walton, LPN back in 2009. After the birth of her son, she found it extremely difficult to get back to her former shape. Though she kept to a strict regimen of diet and exercise, nothing seemed to move the scale.</p>
        <p>Drawing from her medical experience, Nurse Walton sought the help of a trusted physician to work with her in devising a treatment solution. Within 6 weeks, 42 pounds were whisked away and she felt she had finally regained control of her life. The concept of A Better Weigh was then born.</p>
        <p>At A Better Weigh, we see the person, not just a number on the scale. Our compassionate team of physicians and nurses treats every patient with the dignity and respect they deserve. If you've struggled with weight loss in the past, your metabolism may be to blame.</p>
      </div>
      <div class="second-tab">
        <p>Metabolism regulates your ability to shed stubborn pounds and can sabotage your efforts when it feels starved or overworked.</p>
        <p>Many of our patients have endured the monumental challenges of traditional weight loss and turned to A Better Weigh in search of a more practical solution. We help you to find success through our use of FDA-approved medications and lipotropic formulas, which allow the body to switch over from storage to fat burning mode.</p>
        <p>We are always thrilled to celebrate patient achievements as you reach your most meaningful milestones, whether that be slipping into an old pair of jeans from high school or flaunting a set of stunning before and after photos. If you're considering medical weight loss, but are unsure if it’s the right choice for you, we welcome you to speak with our team. Our mission is to equip you with the guidance and information you need to achieve your greatest quality of life.</p>
      </div>
    </div>
    <div class="img-wrap">
      <img src="@asset('images/about/story.png')" alt="Our Story" class="img-fluid"/>
      <a href="#" class="color-btn">Talk With Our Team</a>
    </div>
  </div>
  <div class="decor-bottom">
    <svg width="437" height="437" viewBox="0 0 437 437" fill="none" xmlns="http://www.w3.org/2000/svg">
      <circle opacity="0.15" cx="218.5" cy="218.5" r="204.5" stroke="white" stroke-width="28"/>
    </svg>      
  </div>
  <div class="decor-right">
    <svg width="150" height="150" viewBox="0 0 150 150" fill="none" xmlns="http://www.w3.org/2000/svg">
      <circle opacity="0.15" cx="75" cy="75" r="61" stroke="white" stroke-width="28"/>
    </svg>      
  </div>
  <div class="decor-upper">
    <svg width="208" height="437" viewBox="0 0 208 437" fill="none" xmlns="http://www.w3.org/2000/svg">
      <circle opacity="0.15" cx="-10.5" cy="218.5" r="204.5" stroke="white" stroke-width="28"/>
    </svg>      
  </div>
</div>