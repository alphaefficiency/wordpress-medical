<div class="container-fluid header">
  <div class="container">
    <div class="header-text">
      <h1>Get To Know, The Better Way To Lose Weight</h1>
      <p>Medical weight loss brings faster, safer, and lasting results. Find out how A Better Weigh helps you lose excess fat without the pain of traditional dieting.</p>
    </div>
    <div class="img-wrap">
      <img src="@asset('images/about/hero.png')" alt="Hero Image" class="img-fluid"/>
    </div>
  </div>
</div>