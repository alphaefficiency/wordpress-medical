<div class="container-fluid your-body">
  <div class="container">
    <h3 class="title" style="color: #333;">Your Body, Made More Powerful</h3>
    <p>Check out our easy-to-start programs. Each program includes effective, FDA-approved treatment to stimulate your body's metabolism and accelerate its fat-burning process.</p>
    <div class="your-body-main">
      <div class="item basic">
        <p class="item-title">Basic Program</p>
        <span>LOSE 4-7 LBS IN 2 WEEKS</span>
        <ul>
          <li>Physician Supervised</li>
          <li>Vitamin B12 Injectin</li>
          <li>Appetite Supperssant</li>
          <li>Mineral Supplement</li>
          <li>Individual Consultation</li>
        </ul>
        <a href="{{get_site_url()}}/ourprograms#basicprogram" class="color-btn">Learn More</a>
      </div>
      <div class="item lipo-ignite">
        <span class="most-pop">MOST POPULAR</span>
        <p class="item-title">Lipo-Ignite Program</p>
        <span>LOSE 2 TO 3 LBS / INCHES A WEEK</span>
        <ul>
          <li>Physician Supervised</li>
          <li>VLipotropic Injection</li>
          <li>Fat Burner</li>
          <li>Vitamin B Complex</li>
          <li>3 Essentials Amino Acids</li>
        </ul>
        <a href="{{get_site_url()}}/ourprograms#lipoignite" class="color-btn">Learn More</a>
      </div>
      <div class="item elite">
        <p class="item-title">Elite Program</p>
        <span>LOSE 20 TO 40 LBS IN 6 WEEKS</span>
        <ul>
          <li>Physician Supervised</li>
          <li>VScheduled Orientation</li>
          <li>Low Calorie Diet</li>
          <li>Program Packet</li>
          <li>Individual Consultation</li>
        </ul>
        <a href="{{get_site_url()}}/ourprograms#eliteprogram" class="color-btn">Learn More</a>
      </div>
      <div class="item crave-x">
        <p class="item-title">Crave-X Program</p>
        <span>Eat less & reduce stress</span>
        <ul>
          <li>Physician Supervised</li>
          <li>Individual consultation</li>
          <li>Nutritional coaching and support</li>
          <li>Crave-X capsules</li>
          {{-- <li>Individual Consultation</li> --}}
        </ul>
        <a href="{{get_site_url()}}/ourprograms#cravexprograms" class="color-btn">Learn More</a>
      </div>
    </div>
  </div>
  <div class="decor decor-l">
    <svg width="196" height="211" viewBox="0 0 196 211" fill="none" xmlns="http://www.w3.org/2000/svg">
      <circle cx="90.5" cy="105.5" r="91.5" stroke="white" stroke-width="28"/>
    </svg>      
  </div>
  <div class="decor decor-r">
    <svg width="310" height="348" viewBox="0 0 310 348" fill="none" xmlns="http://www.w3.org/2000/svg">
      <circle cx="174" cy="174" r="160" stroke="white" stroke-width="28"/>
    </svg>      
  </div>
</div>