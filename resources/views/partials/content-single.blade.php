<?php 
  $tags = get_the_tags();
  $url = 'http' . ( isset( $_SERVER['HTTPS'] ) ? 's' : '' ) . '://' . "{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
?>

<div class="container-fluid single-post-wrapper">
  <div class="gray"></div>
  <article class="container single-blog" @php post_class() @endphp>
    <div class="entry-content">
      <div class="blog-main-body">
        <div class="blog-title">
          @if (has_post_thumbnail($post->ID))
            <?=get_the_post_thumbnail($post->ID, 'full')?>
          @elseif (!$attachments)
            <img class="post-thumbnail" src="@asset('images/blog/default-wide.png')" alt="Post image">
          @else
            <img class="post-thumbnail" src="<?=$attachments[0]->guid?>" alt="Post image"/>
          @endif
          <div class="title-wrap">
            <h1>{!! get_the_title() !!}</h1>
            <p> <?= get_the_date('F j. Y', $post->ID)?></p>
          </div>
        </div>
        @php the_content() @endphp
        <div class="after-blog">
          @if($tags)
          <div class="tags">
            <p>Tags</p>
            @foreach ($tags as $tag)
              <a href="{{get_site_url()}}/tag/<?=$tag->slug?>"><?=$tag->name?></a>
            @endforeach
          </div>
          @endif
          <div class="share">
            <p>Share</p>
            {{-- <a href="#" target="_blank">
              <svg width="19" height="19" viewBox="0 0 19 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M9.49745 6.33198C7.75305 6.33198 6.32949 7.75557 6.32949 9.5C6.32949 11.2444 7.75305 12.668 9.49745 12.668C11.2418 12.668 12.6654 11.2444 12.6654 9.5C12.6654 7.75557 11.2418 6.33198 9.49745 6.33198ZM18.9989 9.5C18.9989 8.18811 19.0108 6.88811 18.9371 5.5786C18.8635 4.05757 18.5165 2.70766 17.4043 1.59541C16.2897 0.480781 14.9421 0.136173 13.4211 0.0624986C12.1093 -0.0111762 10.8093 0.000706907 9.49982 0.000706907C8.18796 0.000706907 6.88798 -0.0111762 5.5785 0.0624986C4.0575 0.136173 2.70761 0.483158 1.59538 1.59541C0.480772 2.71004 0.136171 4.05757 0.0624975 5.5786C-0.0111759 6.89049 0.000706894 8.19049 0.000706894 9.5C0.000706894 10.8095 -0.0111759 12.1119 0.0624975 13.4214C0.136171 14.9424 0.483149 16.2923 1.59538 17.4046C2.70999 18.5192 4.0575 18.8638 5.5785 18.9375C6.89036 19.0112 8.19034 18.9993 9.49982 18.9993C10.8117 18.9993 12.1117 19.0112 13.4211 18.9375C14.9421 18.8638 16.292 18.5168 17.4043 17.4046C18.5189 16.29 18.8635 14.9424 18.9371 13.4214C19.0132 12.1119 18.9989 10.8119 18.9989 9.5ZM9.49745 14.3744C6.80005 14.3744 4.62312 12.1974 4.62312 9.5C4.62312 6.80255 6.80005 4.62558 9.49745 4.62558C12.1948 4.62558 14.3718 6.80255 14.3718 9.5C14.3718 12.1974 12.1948 14.3744 9.49745 14.3744ZM14.5714 5.56434C13.9416 5.56434 13.433 5.05575 13.433 4.42595C13.433 3.79615 13.9416 3.28755 14.5714 3.28755C15.2012 3.28755 15.7098 3.79615 15.7098 4.42595C15.71 4.5755 15.6806 4.72361 15.6235 4.86181C15.5664 5.00001 15.4825 5.12558 15.3768 5.23133C15.271 5.33708 15.1455 5.42093 15.0073 5.47807C14.8691 5.53521 14.721 5.56453 14.5714 5.56434Z" fill="#ACACAC"/>
              </svg>                
            </a> --}}
            <a href="https://twitter.com/intent/tweet?text=<?php echo $title; ?>&amp;url=<?php echo $url; ?>" target="_blank">
              <svg width="20" height="17" viewBox="0 0 20 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M20 1.92375C19.2563 2.25 18.4637 2.46625 17.6375 2.57125C18.4875 2.06375 19.1363 1.26625 19.4412 0.305C18.6488 0.7775 17.7738 1.11125 16.8412 1.2975C16.0887 0.49625 15.0162 0 13.8462 0C11.5763 0 9.74875 1.8425 9.74875 4.10125C9.74875 4.42625 9.77625 4.73875 9.84375 5.03625C6.435 4.87 3.41875 3.23625 1.3925 0.7475C1.03875 1.36125 0.83125 2.06375 0.83125 2.82C0.83125 4.24 1.5625 5.49875 2.6525 6.2275C1.99375 6.215 1.3475 6.02375 0.8 5.7225C0.8 5.735 0.8 5.75125 0.8 5.7675C0.8 7.76 2.22125 9.415 4.085 9.79625C3.75125 9.8875 3.3875 9.93125 3.01 9.93125C2.7475 9.93125 2.4825 9.91625 2.23375 9.86125C2.765 11.485 4.2725 12.6788 6.065 12.7175C4.67 13.8088 2.89875 14.4662 0.98125 14.4662C0.645 14.4662 0.3225 14.4513 0 14.41C1.81625 15.5813 3.96875 16.25 6.29 16.25C13.835 16.25 17.96 10 17.96 4.5825C17.96 4.40125 17.9538 4.22625 17.945 4.0525C18.7588 3.475 19.4425 2.75375 20 1.92375Z" fill="#ACACAC"/>
              </svg>                
            </a>
            <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $url; ?>" target="_blank">
              <svg width="10" height="20" viewBox="0 0 10 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M2.55277 20V10.6154H0V7.23652H2.55277V4.3505C2.55277 2.08264 4.0607 0 7.53529 0C8.9421 0 9.98237 0.1311 9.98237 0.1311L9.9004 3.28642C9.9004 3.28642 8.83949 3.27638 7.68178 3.27638C6.42879 3.27638 6.22804 3.83768 6.22804 4.7693V7.23652H10L9.83588 10.6154H6.22804V20H2.55277Z" fill="#ACACAC"/>
              </svg>                
            </a>
          </div>
        </div>
        <div class="author">
          <div class="img-wrap">
            <img src="@asset('images/blog/michelle-single.png')" alt="Nurse Walton" class="img-fluid">
          </div>
          <div class="author-desc">
            <div class="author-upper">
              <p class="nadname">Author</p>
              <p class="name">Nurse Walton</p>
            </div>
            <p>Born and raised in Chicago, IL, Chanay received her Practical Nurse licensure and went to work 
              in clinical specialties such as Home Health, Assisted Living, Long-Term Care and Dialysis Centers. 
              Through this work, she realized the importance of diet, nutrition and weight loss among her patients.
               This led her to open A Better Weigh, Inc. Medical Weight Loss Center in 2009.</p>
          </div>
        </div>
      </div>
      @include('components.blog-aside')
    </div>
    
    @php comments_template('/partials/comments.blade.php') @endphp
  </article>
</div>
