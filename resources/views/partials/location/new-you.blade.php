<div class="container-fluid new-you">
  <div class="container">
    <div class="new-you-frame">
      <h2 class="title" style="color: #333;">Start Seeing a Whole New You!</h2>
      <p>We provide a weight loss program that includes strong coaching support and education to promote lifelong solutions for managing your weight.</p>
      <a href="https://betterweighmedical.com/book-online-now" target="_blank" class="color-btn">Get Started Today</a>
    </div>
  </div>
</div>
<div class="container-fluid new-you-mobile">
  <div class="container">
    <div class="new-you-frame">
      <h2 class="title" style="color: #333;">Start Seeing a Whole New You!</h2>
      <p>We provide a weight loss program that includes strong coaching support and education to promote lifelong solutions for managing your weight.</p>
      <a href="https://betterweighmedical.com/book-online-now" target="_blank" class="color-btn">Get Started Today</a>
    </div>
  </div>
</div>