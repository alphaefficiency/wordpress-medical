<div class="container-fluid locations-header">
  <div class="container">
    <div class="header-wrap">
      <h1>Our Locations</h1>
      <p>3 locations to serve you, and many more to come.</p>
    </div>
    <p>You’re one step closer to changing your life, forever! Our medical team looks forward to working with you to help you reach your goal weight and transition to a healthier lifestyle.</p>
  </div>
</div>