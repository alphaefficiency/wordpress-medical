<div class="container-fluid covid">
  <div class="container">
    <h2 class="title">What We Are Doing About Covid 19</h2>
    <div class="covid-text-wrap">
      <div class="tab">
        <p>With the recent developments around the coronavirus disease (COVID-19), we are taking special measures to ensure the safety of our customers and staff.</p>
        <p>A Better Weigh, Inc is committed to maintaining a clean and healthy environment. As a result, we want you to know that our clinics will remain open to serve you.</p>
        <p>We are closely monitoring recommendations by the WHO, CDC, Chicago Health Dept., Illinois Dept. of Public Health, and the Cook County Health Dept.</p>
        <p>And we have implemented the following safety precautions:</p>
        <ul>
          <li>Reducing the amount of lobby seating and spacing out chairs to help with social distancing</li>
          <li>Fast-tracking physical exams to significantly reduce close up person to person contact</li>
          <li>Frequently disinfecting surfaces in high traffic areas of the clinic</li>
        </ul>
        <p>We ask that everyone adhere to the following recommendations:</p>
        <ul>
          <li>Frequent hand washing and avoiding personal contact</li>
          <li>Avoid touching face, eyes, and ears</li>
          <li>Cover mouth during sneezing and/or coughing</li>
          <li>Stay home if feeling symptoms of sore throat, fever, or body aches</li>
        </ul>
      </div>
      <div class="tab">
        <p>Obesity places Americans at a greater risk for many diseases known to lead to complications in many people infected by coronavirus.</p>
        <p>Therefore we are committed to helping our patients manage their weight and overall health during this unfortunate pandemic as safely as possible.</p>
        <p>While it has always been our goal to provide excellent service to the Chicagoland area,</p>
        <p>we are also committed to being a safe, responsible, and sanitary establishment where our patients feel secure that their health is of utmost importance to us!</p>
        <p>We thank you for your continued patronage and look forward to seeing you soon!</p>
        <span>Nurse Walton</span>
      </div>
    </div>
  </div>
  <div class="decor">
    <svg width="284" height="394" viewBox="0 0 284 394" fill="none" xmlns="http://www.w3.org/2000/svg">
      <circle opacity="0.15" cx="197" cy="197" r="183" stroke="white" stroke-width="28"/>
    </svg>      
  </div>
</div>