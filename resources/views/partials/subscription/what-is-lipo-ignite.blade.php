<div class="container-fluid what-is-lipo">
  <div class="container">
    <h2 class="title" style="color: #333;">What is Lipo-Ignite&copy;</h2>
    <p class="subtitle">Lose 2-3 lbs a Week Without Sweating or Straining</p>
    <div class="lipo-text">
      <div class="tab first-tab">
        <p>Lipo-Ignite are safe FDA approved ‘lipotropic’ injections for fast and effective weight loss. These injections boost your body's natural metabolism so you automatically burn fat without having to do anything else.</p>
        <p>These injections are jam-packed with super-nutrients like:</p>
        <ul>
          <li>Choline “The fat emulsifier”</li>
          <li>Inositol “The fat metabolizer”</li>
          <li>And Myoden “The calorie blaster”</li>
        </ul>
        <p>Lipo-Ignite also has, what could be called the ‘wonder nutrient’, ‘Methionine’ which, once inside the body, turns into the youth-enhancing and waist-slimming nutrients:</p>
        <ul>
          <li>Glutathione</li>
          <li>S-adenosylmethionine</li>
          <li>Taurine & Creatine</li>
        </ul>
      </div>
      <div class="tab second-tab">
        <p>Lipo-Ignite injections are relatively pain-free and are injected by the Doctors and Nurses that supervise and support your weight loss journey when you join us at A Better Weigh in Chicago.</p>
        <p class="bold-text">Here is what the injections help you accomplish:</p>
        <ul>
          <li>Metabolize fat and remove it from your liver.</li>
          <li>Improve your energy and mood -Suppresses appetite</li>
          <li>Lowers your cholesterol (emulsify fat) and improve brain function</li>
          <li>Decreases water weight</li>
        </ul>
        <p>This help you:</p>
        <ul>
          <li>Feel more energetic</li>
          <li>Look younger</li>
          <li>Feel more confident</li>
          <li>Live Longer</li>
        </ul>
        <img src="@asset('images/subscription/approved.png')" alt="FDA Approved"/>
      </div>
    </div>
    <div class="lipo-testimonials">
      <div class="lipo-testimonial">
        <div class="image-wrap">
          <p class="before">Before</p>
          <p class="after">After</p>
          <img src="@asset('images/subscription/Chanay.png')" alt="Chanay" class="img-fluid"/>
        </div>
        <div class="bottom">
          <p class="lost-amount">LOST 42 POUNDS!</p>
          <p class="name">Chanay</p>
        </div>
      </div>
      <div class="lipo-testimonial">
        <div class="image-wrap">
          <p class="before">Before</p>
          <p class="after">After</p>
          <img src="@asset('images/subscription/Shequita.png')" alt="Shequita" class="img-fluid"/>
        </div>
        <div class="bottom">
          <p class="lost-amount">LOST 57 POUNDS!</p>
          <p class="name">Shequita</p>
        </div>
      </div>
      <div class="lipo-testimonial">
        <div class="image-wrap">
          <p class="before">Before</p>
          <p class="after">After</p>
          <img src="@asset('images/subscription/nick.png')" alt="Nick" class="img-fluid"/>
        </div>
        <div class="bottom">
          <p class="lost-amount">LOST 72 POUNDS!</p>
          <p class="name">Nick</p>
        </div>
      </div>
    </div>
    <a href="https://betterweigh.kartra.com/page/EG281" target="_blank" class="color-btn">Order Now</a>
  </div>
</div>