<div class="payment-modal" id="popup_c594e0666b2ca9e99406cfec85c8e606" style="display: none">
  {{-- <iframe src="https://app.kartra.com/product/order_modal/c594e0666b2ca9e99406cfec85c8e606?type=checkout&amp;owner=Or0EvBpJ&amp;value=c594e0666b2ca9e99406cfec85c8e606" id="the_frame_c594e0666b2ca9e99406cfec85c8e606" frameborder="0" name="the_frame_c594e0666b2ca9e99406cfec85c8e606" scrolling="no" style="max-width: 500px; width: 100vw; z-index: 1000000;"></iframe> --}}
  <script type="text/javascript" id='pw_6039314480784' src="https://app.paywhirl.com/pwa.js"></script>
<script>paywhirl('widget',{autoscroll: 1, initial_autoscroll: 0, domain:'a-better-weigh', uuid:'2e9c9322-ac94-44c1-a9ec-3dae8fe71cee'},'pw_6039314480784'); </script>
  <div class="close-modal">
    <div class="x-btn">X</div>
  </div>
</div>

<div class="container-fluid header">
  <div class="container-fluid upper-header">
    <h1>Save Over 50% Off Lipo-Ignite&copy; With The Monthly Subscription</h1>
    <div class="timer">
      <div class="item">
        <p>Days</p>
        <p id="days" class="timer-int"></p>
      </div>
      <svg width="5" height="40" viewBox="0 0 5 40" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M2.825 3.9C2.325 3.9 1.875 3.7 1.475 3.3C1.075 2.9 0.875 2.45 0.875 1.95C0.875 1.4 1.075 0.95 1.475 0.599998C1.875 0.199997 2.325 -3.03984e-06 2.825 -3.03984e-06C3.375 -3.03984e-06 3.825 0.199997 4.175 0.599998C4.575 0.95 4.775 1.4 4.775 1.95C4.775 2.45 4.575 2.9 4.175 3.3C3.825 3.7 3.375 3.9 2.825 3.9ZM2.825 39.225C2.325 39.225 1.875 39.025 1.475 38.625C1.075 38.225 0.875 37.775 0.875 37.275C0.875 36.725 1.075 36.275 1.475 35.925C1.875 35.525 2.325 35.325 2.825 35.325C3.375 35.325 3.825 35.525 4.175 35.925C4.575 36.275 4.775 36.725 4.775 37.275C4.775 37.775 4.575 38.225 4.175 38.625C3.825 39.025 3.375 39.225 2.825 39.225Z" fill="#333333"/>
      </svg>        
      <div class="item">
        <p>Hours</p>
        <p id="hours" class="timer-int"></p>
      </div>
      <svg width="5" height="40" viewBox="0 0 5 40" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M2.825 3.9C2.325 3.9 1.875 3.7 1.475 3.3C1.075 2.9 0.875 2.45 0.875 1.95C0.875 1.4 1.075 0.95 1.475 0.599998C1.875 0.199997 2.325 -3.03984e-06 2.825 -3.03984e-06C3.375 -3.03984e-06 3.825 0.199997 4.175 0.599998C4.575 0.95 4.775 1.4 4.775 1.95C4.775 2.45 4.575 2.9 4.175 3.3C3.825 3.7 3.375 3.9 2.825 3.9ZM2.825 39.225C2.325 39.225 1.875 39.025 1.475 38.625C1.075 38.225 0.875 37.775 0.875 37.275C0.875 36.725 1.075 36.275 1.475 35.925C1.875 35.525 2.325 35.325 2.825 35.325C3.375 35.325 3.825 35.525 4.175 35.925C4.575 36.275 4.775 36.725 4.775 37.275C4.775 37.775 4.575 38.225 4.175 38.625C3.825 39.025 3.375 39.225 2.825 39.225Z" fill="#333333"/>
      </svg>   
      <div class="item">
        <p>Minutes</p>
        <p id="minutes" class="timer-int"></p>
      </div>  
      <svg width="5" height="40" viewBox="0 0 5 40" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M2.825 3.9C2.325 3.9 1.875 3.7 1.475 3.3C1.075 2.9 0.875 2.45 0.875 1.95C0.875 1.4 1.075 0.95 1.475 0.599998C1.875 0.199997 2.325 -3.03984e-06 2.825 -3.03984e-06C3.375 -3.03984e-06 3.825 0.199997 4.175 0.599998C4.575 0.95 4.775 1.4 4.775 1.95C4.775 2.45 4.575 2.9 4.175 3.3C3.825 3.7 3.375 3.9 2.825 3.9ZM2.825 39.225C2.325 39.225 1.875 39.025 1.475 38.625C1.075 38.225 0.875 37.775 0.875 37.275C0.875 36.725 1.075 36.275 1.475 35.925C1.875 35.525 2.325 35.325 2.825 35.325C3.375 35.325 3.825 35.525 4.175 35.925C4.575 36.275 4.775 36.725 4.775 37.275C4.775 37.775 4.575 38.225 4.175 38.625C3.825 39.025 3.375 39.225 2.825 39.225Z" fill="#333333"/>
      </svg>
      <div class="item">
        <p>Seconds</p>
        <p id="seconds" class="timer-int"></p>
      </div>        
    </div>
  </div>
  <div class="container-fluid lower-header">
    <div class="container">
      <div class="text-wrap">
        <h2 class="title">Lipo-Ignite will help you lose 2-3 pounds a week.</h2>
        <p>Get 4 injections a month on our monthly subscription and save over 50%.</p>
        <p>Note: Injections <span>DO NOT</span> rollover. They must be used within the month.</p>
        <strong class="regular-price">Regularly $140 Limited Time Subscription Price Only $69/mo</strong>
        {{-- <a href="https://betterweigh.kartra.com/page/EG281" target="_blank" class="color-btn">Order Now</a> --}}
        <a href="#" class="color-btn">Order Now</a>
        <p>You can cancel anytime.</p>
        <img src="@asset('images/subscription/card-types.png')" alt="Available Cards"/>
      </div>
      <div class="img-wrap">
        <img src="@asset('images/subscription/header.png')" alt="Lose Pounds Per Week" class="img-fluid"/>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid belt">
  <div class="container">
    <div class="item">
      <svg width="45" height="45" viewBox="0 0 45 45" fill="none" xmlns="http://www.w3.org/2000/svg">
        <circle cx="22.5" cy="22.5" r="22.5" fill="url(#paint0_linear)"/>
        <path d="M25.25 18.375C24.836 18.375 24.5 18.039 24.5 17.625V16.5H21.5V17.625C21.5 18.039 21.164 18.375 20.75 18.375C20.336 18.375 20 18.039 20 17.625V16.5C20 15.6727 20.6727 15 21.5 15H24.5C25.3273 15 26 15.6727 26 16.5V17.625C26 18.039 25.664 18.375 25.25 18.375Z" fill="white"/>
        <path d="M23.5325 25.035C23.3975 25.0875 23.2025 25.125 23 25.125C22.7975 25.125 22.6025 25.0875 22.4225 25.02L14 22.215V27.9375C14 29.0775 14.9225 30 16.0625 30H29.9375C31.0775 30 32 29.0775 32 27.9375V22.215L23.5325 25.035Z" fill="white"/>
        <path d="M32 19.3125V21.03L23.18 23.97C23.12 23.9925 23.06 24 23 24C22.94 24 22.88 23.9925 22.82 23.97L14 21.03V19.3125C14 18.1725 14.9225 17.25 16.0625 17.25H29.9375C31.0775 17.25 32 18.1725 32 19.3125Z" fill="white"/>
        <defs>
        <linearGradient id="paint0_linear" x1="0" y1="0" x2="45" y2="45" gradientUnits="userSpaceOnUse">
        <stop stop-color="#FFD900"/>
        <stop offset="1" stop-color="#DD71C6"/>
        </linearGradient>
        </defs>
      </svg>
      <div class="belt-text">
        <h2>10+</h2>
        <p>Years in Business</p>
      </div>    
    </div>
    <div class="item">
      <svg width="45" height="45" viewBox="0 0 45 45" fill="none" xmlns="http://www.w3.org/2000/svg">
        <circle cx="22.5" cy="22.5" r="22.5" fill="url(#paint0_linear)"/>
        <path d="M22.5 14L19.8726 19.5979L14 20.4904L18.25 24.8496L17.2452 31L22.5 28.0979L27.7548 31L26.75 24.8496L31 20.4964L25.1274 19.5979L22.5 14Z" fill="white"/>
        <defs>
        <linearGradient id="paint0_linear" x1="0" y1="0" x2="45" y2="45" gradientUnits="userSpaceOnUse">
        <stop stop-color="#FFD900"/>
        <stop offset="1" stop-color="#DD71C6"/>
        </linearGradient>
        </defs>
      </svg>        
      <div class="belt-text">
        <h2>2000+</h2>
        <p>Positive Reviews</p>
      </div>    
    </div>
    <div class="item">
      <svg width="45" height="45" viewBox="0 0 45 45" fill="none" xmlns="http://www.w3.org/2000/svg">
        <circle cx="22.5" cy="22.5" r="22.5" fill="url(#paint0_linear)"/>
        <path d="M14.3125 30.6876H16.1875C16.9113 30.6876 17.5 30.0988 17.5 29.375V20.75C17.5 20.0263 16.9113 19.4375 16.1875 19.4375H14.3125C13.5888 19.4375 13 20.0263 13 20.75V29.375C13 30.0988 13.5888 30.6876 14.3125 30.6876Z" fill="white"/>
        <path d="M22.5858 14C21.8358 14 21.4608 14.375 21.4608 16.25C21.4608 18.032 19.735 19.466 18.625 20.2048V29.4958C19.8258 30.0516 22.2295 30.8751 25.9608 30.8751H27.1608C28.6233 30.8751 29.8683 29.8251 30.1158 28.3851L30.9558 23.51C31.2708 21.6725 29.8608 20 28.0008 20H24.4608C24.4608 20 25.0233 18.875 25.0233 17C25.0233 14.75 23.3358 14 22.5858 14Z" fill="white"/>
        <defs>
        <linearGradient id="paint0_linear" x1="0" y1="0" x2="45" y2="45" gradientUnits="userSpaceOnUse">
        <stop stop-color="#FFD900"/>
        <stop offset="1" stop-color="#DD71C6"/>
        </linearGradient>
        </defs>
      </svg>              
      <div class="belt-text">
        <h2>15000+</h2>
        <p>Satisfied Clients</p>
      </div>    
    </div>
    <div class="item" style="width: 220px;">
      <svg width="45" height="45" viewBox="0 0 45 45" fill="none" xmlns="http://www.w3.org/2000/svg">
        <circle cx="22.5" cy="22.5" r="22.5" fill="url(#paint0_linear)"/>
        <path d="M22.9686 15.6554C18.6204 11.8523 13.0918 15.1938 13.0011 19.7238C12.978 20.8782 13.3113 22.0028 13.9746 23.0395H18.2743L19.0391 21.7649C19.2611 21.3948 19.7985 21.3829 20.0332 21.7518L21.6467 24.2874L23.9988 19.322C24.2039 18.8888 24.8152 18.875 25.0414 19.2951L27.0576 23.0395H31.9625C35.5959 17.3609 28.4151 10.8917 22.9686 15.6554Z" fill="white"/>
        <path d="M26.1934 23.9009L24.5597 20.8667L22.2494 25.744C22.0528 26.1589 21.4747 26.195 21.2282 25.8075L19.5541 23.1769L19.1056 23.9245C19 24.1005 18.8098 24.2082 18.6045 24.2082H14.8984C15.0146 24.3298 14.3954 23.7118 22.5557 31.8298C22.7836 32.0566 23.1521 32.0567 23.38 31.8298C31.4147 23.8367 30.9214 24.3295 31.0373 24.2082H26.708C26.493 24.2082 26.2954 24.0902 26.1934 23.9009Z" fill="white"/>
        <defs>
        <linearGradient id="paint0_linear" x1="0" y1="0" x2="45" y2="45" gradientUnits="userSpaceOnUse">
        <stop stop-color="#FFD900"/>
        <stop offset="1" stop-color="#DD71C6"/>
        </linearGradient>
        </defs>
      </svg>               
      <div class="belt-text">
        <h2>Many+</h2>
        <p>Nurses/Physician Assistants On Staff</p>
      </div>    
    </div>
  </div>
</div>