<div class="container-fluid customer-reviews">
  <div class="container">
    <div class="left-section">
      <h3 class="title">Customer Reviews</h3>
      <p>From when we previously sold Lipo-Ignite&copy; on Groupon</p>
      <img src="@asset('images/subscription/customer-reviews.png')" alt="Google Reviews" class="img-fluid"/>
    </div>
    <div class="right-section">
      <h3 class="title">Here are some of our reviews from Google!</h3>
      <img src="@asset('images/subscription/review1.png')" alt="Google Reviews" class="img-fluid"/>
      <img src="@asset('images/subscription/review2.png')" alt="Google Reviews" class="img-fluid"/>
      <a href="https://betterweigh.kartra.com/page/EG281" target="_blank" class="color-btn">Order Now</a>
    </div>
  </div>
</div>