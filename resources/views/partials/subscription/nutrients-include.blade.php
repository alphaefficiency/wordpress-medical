<div class="container-fluid nutrients-include">
  <div class="container">
    <h3 class="title">Nutrients in Lipo-Ignite&copy; Include</h3>
    <div class="nutrients-wrap">
      <div class="list-frame">
        <div class="nutrient-item nutrient-item-active" data-id="1">
          <p>1. Choline</p>
          <svg width="12" height="20" viewBox="0 0 12 20" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0.0011824 18.2121L1.79916 20L11.7891 10L1.78906 8.74228e-07L0.00118096 1.78788L8.2133 10L0.0011824 18.2121Z" fill="white"/>
          </svg>                       
        </div>
        <div class="nutrient-item" data-id="2">
          <p>2. Inositol</p>
          <svg width="12" height="20" viewBox="0 0 12 20" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0.0011824 18.2121L1.79916 20L11.7891 10L1.78906 8.74228e-07L0.00118096 1.78788L8.2133 10L0.0011824 18.2121Z" fill="white"/>
          </svg>                       
        </div>
        <div class="nutrient-item" data-id="3">
          <p>3. Methionine</p>
          <svg width="12" height="20" viewBox="0 0 12 20" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0.0011824 18.2121L1.79916 20L11.7891 10L1.78906 8.74228e-07L0.00118096 1.78788L8.2133 10L0.0011824 18.2121Z" fill="white"/>
          </svg>                       
        </div>
        <div class="nutrient-item" data-id="4">
          <p>4. Vitamin B Complex  <br style="display: none"/>(Includes B12 & B6)</p>
          <svg width="12" height="20" viewBox="0 0 12 20" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0.0011824 18.2121L1.79916 20L11.7891 10L1.78906 8.74228e-07L0.00118096 1.78788L8.2133 10L0.0011824 18.2121Z" fill="white"/>
          </svg>                       
        </div>
        <div class="nutrient-item" data-id="5">
          <p>5. Myoden (AMP)</p>
          <svg width="12" height="20" viewBox="0 0 12 20" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0.0011824 18.2121L1.79916 20L11.7891 10L1.78906 8.74228e-07L0.00118096 1.78788L8.2133 10L0.0011824 18.2121Z" fill="white"/>
          </svg>                       
        </div>
        <div class="nutrient-item" data-id="6">
          <p>6. CO-Q10, Chromium and Carnitine</p>
          <svg width="12" height="20" viewBox="0 0 12 20" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0.0011824 18.2121L1.79916 20L11.7891 10L1.78906 8.74228e-07L0.00118096 1.78788L8.2133 10L0.0011824 18.2121Z" fill="white"/>
          </svg>                       
        </div>
      </div>
      <div class="nutrient-description-wrap">
        <div class="nutrient-description nutrient-desc-active" data-id="1">
          <p>Many people who struggle with their weight don’t realize that they are also low in choline.</p>
          <p>It is a vital nutrient that is required not just for weight loss, but for optimal health. It is however, very hard to find in most supplements because it is water soluble. Without it, fat and cholesterol can build up in your liver, and cause you serious health problems.</p>
          <p>Lipo-Ignite is the most effective and easiest place to get this fat removing nutrient. Choline also impacts liver function, healthy brain development, muscle movement, your nervous system and most importantly your fat burning metabolism.</p>
          <p>Choline is like a master transportation system that busses fat and cholesterol out of your body leaving your thinner and happier.</p>
        </div>
        <div class="nutrient-description" data-id="2">
          <p>Inositol is another vitamin-like substance we use in Lipo-Ignite to speed up your weight loss. It helps your body in so many ways.</p>
          <p>But here are some that I like:</p>
          <p>A. Inositol works with serotonin, the brain’s happy hormone. So it  will help you feel better and happier while you are losing weight.</p>
          <p>B. Inositol helps with insulin resistance and your metabolism. Insulin resistance is not just a problem for people with diabetes it is also the main underlying cause for getting fat</p>
          <p>In order to lose weight and keep it off you need to fix your resistance to insulin. Inositol helps with just that. It not only stops you from gaining more weight but also helps you lose the extra pounds you already have.</p>
        </div>
        <div class="nutrient-description" data-id="3">
          <p>Methionine is the only sulfur-containing amino acid that your body cannot produce. It is the base nutrient that your body needs to become more youthful and vibrant. But, more importantly it is super important  to support your safe and easy metabolic weight loss.</p>
          <p>Methionine is not only a life preserving antioxidant that helps to protect your damaged tissues, it is also converted inside your body to:</p>
          <div class="expandables">
            <div class="expandable-item">
              <div class="item-title">
                <p>A. Glutathione</p>
                <svg width="13" height="8" viewBox="0 0 13 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M1.16212 1.38582e-08L8.0848e-08 1.22022L6.5 8L13 1.21337L11.8379 1.41165e-07L6.5 5.57326L1.16212 1.38582e-08Z" fill="white"/>
                </svg>                                  
              </div>
              <div class="item-expand">
                <p>which is known as the body's “Master Antioxidant”. This keeps you looking and feeling younger.</p>
              </div>
            </div>
            <div class="expandable-item">
              <div class="item-title">
                <p>B. S-adenosylmethionine</p>
                <svg width="13" height="8" viewBox="0 0 13 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M1.16212 1.38582e-08L8.0848e-08 1.22022L6.5 8L13 1.21337L11.8379 1.41165e-07L6.5 5.57326L1.16212 1.38582e-08Z" fill="white"/>
                </svg>                                  
              </div>
              <div class="item-expand">
                <p>which is known as the body's “Master Antioxidant”. This keeps you looking and feeling younger.</p>
              </div>
            </div>
            <div class="expandable-item">
              <div class="item-title">
                <p>C. Taurine</p>
                <svg width="13" height="8" viewBox="0 0 13 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M1.16212 1.38582e-08L8.0848e-08 1.22022L6.5 8L13 1.21337L11.8379 1.41165e-07L6.5 5.57326L1.16212 1.38582e-08Z" fill="white"/>
                </svg>                                  
              </div>
              <div class="item-expand">
                <p>which is known as the body's “Master Antioxidant”. This keeps you looking and feeling younger.</p>
              </div>
            </div>
            <div class="expandable-item">
              <div class="item-title">
                <p>D. Creatine</p>
                <svg width="13" height="8" viewBox="0 0 13 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M1.16212 1.38582e-08L8.0848e-08 1.22022L6.5 8L13 1.21337L11.8379 1.41165e-07L6.5 5.57326L1.16212 1.38582e-08Z" fill="white"/>
                </svg>                                  
              </div>
              <div class="item-expand">
                <p>which is known as the body's “Master Antioxidant”. This keeps you looking and feeling younger.</p>
              </div>
            </div>
          </div>
          <p>As you may know, muscle burns more calories than fat, so by building your muscles with creatine, and the other proteins that your body uses Methionine to build, you will actually lose fat even when eating the same amount of food. Methionine is a unique nutrient that helps regenerate and restore your slim figure while giving you more energy, strength and youthful vitality.</p>
        </div>
        <div class="nutrient-description" data-id="4">
          <p>B Vitamins are famous for aiding with weight loss by boosting your metabolism.</p>
          <p>A faster metabolism burns more calories, making a fast metabolism the special secret sauce to losing weight.</p>
          <p>The B vitamins play an essential role for maintaining and boosting body functions (including your metabolism) and supporting good weight control.</p>
        </div>
        <div class="nutrient-description" data-id="5">
          <p>Myoden (AMP)  is a natural chemical found inside all human cells that helps you stay slim and sassy.</p>
          <p>It helps to promote brain function, healthy blood flow circulation, and is fabulous for your skin. It reduces swelling and fluid retention (water weight) and decreases symptoms like itchiness, redness and formation of ulcers. It helps you relax, sleep better and it protects the brain from oxidative stress, which basically means it keeps your brain young and working well.</p>
          <p>All these benefits are great but....</p>
          <p>That is not why we use it in Lipo-Ignite.</p>
          <p>We use it because it is one of our secret superhero ingredients that causes the exact enzyme reactions necessary for proper fat and carbohydrate metabolism. Normally that burger would mostly go straight to your thighs, but by boosting AMP your body uses more of those excess calories for energy instead of simply storing them as fat.</p>
        </div>
        <div class="nutrient-description" data-id="6">
          <p>Lipo-Ignite also includes other essential nutrients and fat burning wonders like CO-Q10, Chromium and Carnitine.</p>
          <p>These are the same supplements that doctors and super fit people use to get in-shape. I could write a book on the benefits of each one of these and how they help you lose weight, but what I think really matters now is that you know Lipo-Ignite includes them and is perfectly formulated to give your body the boost it needs to burn fat fast and keep you feeling young healthy and slim.</p>
        </div>
      </div>
    </div>
    <div class="nutrients-mobile-wrap">
      <div class="nutrient-item" data-id="1">
        <div class="upper">
          <p>1. Choline</p>
          <svg width="20" height="12" viewBox="0 0 20 12" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M1.78788 0.106651L0 1.90463L10 11.8945L20 1.89453L18.2121 0.106651L10 8.31877L1.78788 0.106651Z" fill="white"/>
          </svg>            
        </div>
        <div class="nutrient-description" data-id="1">
          <p>Many people who struggle with their weight don’t realize that they are also low in choline.</p>
          <p>It is a vital nutrient that is required not just for weight loss, but for optimal health. It is however, very hard to find in most supplements because it is water soluble. Without it, fat and cholesterol can build up in your liver, and cause you serious health problems.</p>
          <p>Lipo-Ignite is the most effective and easiest place to get this fat removing nutrient. Choline also impacts liver function, healthy brain development, muscle movement, your nervous system and most importantly your fat burning metabolism.</p>
          <p>Choline is like a master transportation system that busses fat and cholesterol out of your body leaving your thinner and happier.</p>
        </div>
      </div>
      <div class="nutrient-item" data-id="2">
        <div class="upper">
          <p>2. Inositol</p>
          <svg width="20" height="12" viewBox="0 0 20 12" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M1.78788 0.106651L0 1.90463L10 11.8945L20 1.89453L18.2121 0.106651L10 8.31877L1.78788 0.106651Z" fill="white"/>
          </svg>            
        </div>
        <div class="nutrient-description" data-id="4">
          <p>Inositol is another vitamin-like substance we use in Lipo-Ignite to speed up your weight loss. It helps your body in so many ways.</p>
          <p>But here are some that I like:</p>
          <p>A. Inositol works with serotonin, the brain’s happy hormone. So it  will help you feel better and happier while you are losing weight.</p>
          <p>B. Inositol helps with insulin resistance and your metabolism. Insulin resistance is not just a problem for people with diabetes it is also the main underlying cause for getting fat</p>
          <p>In order to lose weight and keep it off you need to fix your resistance to insulin. Inositol helps with just that. It not only stops you from gaining more weight but also helps you lose the extra pounds you already have.</p>
        </div>
      </div>
      <div class="nutrient-item" data-id="3">
        <div class="upper">
          <p>3. Methionine</p>
          <svg width="20" height="12" viewBox="0 0 20 12" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M1.78788 0.106651L0 1.90463L10 11.8945L20 1.89453L18.2121 0.106651L10 8.31877L1.78788 0.106651Z" fill="white"/>
          </svg>                        
        </div>
        <div class="nutrient-description" data-id="3">
          <p>Methionine is the only sulfur-containing amino acid that your body cannot produce. It is the base nutrient that your body needs to become more youthful and vibrant. But, more importantly it is super important  to support your safe and easy metabolic weight loss.</p>
          <p>Methionine is not only a life preserving antioxidant that helps to protect your damaged tissues, it is also converted inside your body to:</p>
          <div class="expandables">
            <div class="expandable-item">
              <div class="item-title">
                <p>A. Glutathione</p>
                <svg width="13" height="8" viewBox="0 0 13 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M1.16212 1.38582e-08L8.0848e-08 1.22022L6.5 8L13 1.21337L11.8379 1.41165e-07L6.5 5.57326L1.16212 1.38582e-08Z" fill="white"/>
                </svg>                                  
              </div>
              <div class="item-expand">
                <p>which is known as the body's “Master Antioxidant”. This keeps you looking and feeling younger.</p>
              </div>
            </div>
            <div class="expandable-item">
              <div class="item-title">
                <p>B. S-adenosylmethionine</p>
                <svg width="13" height="8" viewBox="0 0 13 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M1.16212 1.38582e-08L8.0848e-08 1.22022L6.5 8L13 1.21337L11.8379 1.41165e-07L6.5 5.57326L1.16212 1.38582e-08Z" fill="white"/>
                </svg>                                  
              </div>
              <div class="item-expand">
                <p>which is known as the body's “Master Antioxidant”. This keeps you looking and feeling younger.</p>
              </div>
            </div>
            <div class="expandable-item">
              <div class="item-title">
                <p>C. Taurine</p>
                <svg width="13" height="8" viewBox="0 0 13 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M1.16212 1.38582e-08L8.0848e-08 1.22022L6.5 8L13 1.21337L11.8379 1.41165e-07L6.5 5.57326L1.16212 1.38582e-08Z" fill="white"/>
                </svg>                                  
              </div>
              <div class="item-expand">
                <p>which is known as the body's “Master Antioxidant”. This keeps you looking and feeling younger.</p>
              </div>
            </div>
            <div class="expandable-item">
              <div class="item-title">
                <p>D. Creatine</p>
                <svg width="13" height="8" viewBox="0 0 13 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M1.16212 1.38582e-08L8.0848e-08 1.22022L6.5 8L13 1.21337L11.8379 1.41165e-07L6.5 5.57326L1.16212 1.38582e-08Z" fill="white"/>
                </svg>                                  
              </div>
              <div class="item-expand">
                <p>which is known as the body's “Master Antioxidant”. This keeps you looking and feeling younger.</p>
              </div>
            </div>
          </div>
          <p>As you may know, muscle burns more calories than fat, so by building your muscles with creatine, and the other proteins that your body uses Methionine to build, you will actually lose fat even when eating the same amount of food. Methionine is a unique nutrient that helps regenerate and restore your slim figure while giving you more energy, strength and youthful vitality.</p>
        </div>
      </div>
      <div class="nutrient-item" data-id="4">
        <div class="upper">
          <p>4. Vitamin B Complex  (Includes B12 & B6)</p>
          <svg width="20" height="12" viewBox="0 0 20 12" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M1.78788 0.106651L0 1.90463L10 11.8945L20 1.89453L18.2121 0.106651L10 8.31877L1.78788 0.106651Z" fill="white"/>
          </svg>            
        </div>
        <div class="nutrient-description" data-id="4">
          <p>B Vitamins are famous for aiding with weight loss by boosting your metabolism.</p>
          <p>A faster metabolism burns more calories, making a fast metabolism the special secret sauce to losing weight.</p>
          <p>The B vitamins play an essential role for maintaining and boosting body functions (including your metabolism) and supporting good weight control.</p>
        </div>
      </div>
      <div class="nutrient-item" data-id="5">
        <div class="upper">
          <p>5. Myoden (AMP)</p>
          <svg width="20" height="12" viewBox="0 0 20 12" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M1.78788 0.106651L0 1.90463L10 11.8945L20 1.89453L18.2121 0.106651L10 8.31877L1.78788 0.106651Z" fill="white"/>
          </svg>            
        </div>
        <div class="nutrient-description" data-id="5">
          <p>Myoden (AMP)  is a natural chemical found inside all human cells that helps you stay slim and sassy.</p>
          <p>It helps to promote brain function, healthy blood flow circulation, and is fabulous for your skin. It reduces swelling and fluid retention (water weight) and decreases symptoms like itchiness, redness and formation of ulcers. It helps you relax, sleep better and it protects the brain from oxidative stress, which basically means it keeps your brain young and working well.</p>
          <p>All these benefits are great but....</p>
          <p>That is not why we use it in Lipo-Ignite.</p>
          <p>We use it because it is one of our secret superhero ingredients that causes the exact enzyme reactions necessary for proper fat and carbohydrate metabolism. Normally that burger would mostly go straight to your thighs, but by boosting AMP your body uses more of those excess calories for energy instead of simply storing them as fat.</p>
        </div>
      </div>
      <div class="nutrient-item" data-id="6">
        <div class="upper">
          <p>6. CO-Q10, Chromium and Carnitine</p>
          <svg width="20" height="12" viewBox="0 0 20 12" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M1.78788 0.106651L0 1.90463L10 11.8945L20 1.89453L18.2121 0.106651L10 8.31877L1.78788 0.106651Z" fill="white"/>
          </svg>            
        </div>
        <div class="nutrient-description" data-id="6">
          <p>Lipo-Ignite also includes other essential nutrients and fat burning wonders like CO-Q10, Chromium and Carnitine.</p>
          <p>These are the same supplements that doctors and super fit people use to get in-shape. I could write a book on the benefits of each one of these and how they help you lose weight, but what I think really matters now is that you know Lipo-Ignite includes them and is perfectly formulated to give your body the boost it needs to burn fat fast and keep you feeling young healthy and slim.</p>
        </div>
      </div>
    </div>
  </div>
  <div class="decor decor-l">
    <svg width="257" height="394" viewBox="0 0 257 394" fill="none" xmlns="http://www.w3.org/2000/svg">
      <circle opacity="0.15" cx="60" cy="197" r="183" stroke="white" stroke-width="28"/>
    </svg>      
  </div>
  <div class="decor decor-r">
    <svg width="167" height="203" viewBox="0 0 167 203" fill="none" xmlns="http://www.w3.org/2000/svg">
      <circle opacity="0.15" cx="101.5" cy="101.5" r="87.5" stroke="white" stroke-width="28"/>
    </svg>      
  </div>
</div>