<div class="container-fluid first-step">
  <div class="decor">
    <svg width="472" height="472" viewBox="0 0 472 472" fill="none" xmlns="http://www.w3.org/2000/svg">
      <circle opacity="0.15" cx="236" cy="236" r="222" stroke="white" stroke-width="28"/>
    </svg>      
  </div>
  <div class="container">
    <div class="first-step-text">
      <h3 class="title">The First Step is All it Takes</h3>
      <p>Let our team of weight loss experts show you how attainable successful results can be. Schedule your free consultation today!</p>
      <a href="https://betterweighmedical.involve.me/start-your-weight-loss-journey-e4b7d1c2b637" class="color-btn">Let’s Do This</a>
    </div>
    <div class="img-wrap">
      <img src="@asset('images/program/first-step.png')" alt="First Step" class="img-fluid"/>
    </div>
  </div>
</div>