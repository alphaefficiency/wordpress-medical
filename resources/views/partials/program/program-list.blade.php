<div class="container-fluid programs-list">
  <div class="gray">
    <div class="container">
      <h1>Find Your Perfect Program</h1>
      <p>Each of our medically assisted weight loss programs is run under the direct supervision of a physician. After
        choosing the right fit for you, your program will be personalized to your unique needs and goals.
        {{-- <a href="https://betterweighmedical.com/quiz" target="blank" class="color-btn">Start a quiz</a> --}}
      </p>
    </div>
  </div>
  <div class="decor decor-l">
    <svg width="238" height="238" viewBox="0 0 238 238" fill="none" xmlns="http://www.w3.org/2000/svg">
      <circle cx="119" cy="119" r="105" stroke="#F9F9F9" stroke-width="28" />
    </svg>
  </div>
  <div class="decor decor-r">
    <svg width="293" height="383" viewBox="0 0 293 383" fill="none" xmlns="http://www.w3.org/2000/svg">
      <circle cx="191.5" cy="191.5" r="177.5" stroke="#F9F9F9" stroke-width="28" />
    </svg>
  </div>
  <div class="decor decor-container">
    <svg width="383" height="383" viewBox="0 0 383 383" fill="none" xmlns="http://www.w3.org/2000/svg">
      <circle cx="191.5" cy="191.5" r="177.5" stroke="#F9F9F9" stroke-width="28" />
    </svg>
  </div>
  <div class="container all-programs">
    <a href="https://betterweighmedical.com/quiz" target="blank" class="color-btn">Program Finder Quiz</a>
    <div class="program">
      <div class="program-tile basic" id="basicprogram">
        <p class="program-title">Basic Program</p>
        <span class="lose-amount">LOSE 4-7 LBS IN 2 WEEKS</span>
        <ul>
          <li>Full physician supervision</li>
          <li>Individual consultation</li>
          <li>Nutritional coaching and support</li>
          <li>Vitamin B-12 injection</li>
          <li>Appetite suppressant supplement</li>
          <li>Chromium Picolinate carb blocker</li>
          <li>Blood pressure-regulating diuretic</li>
          <li>Optional upgrade to get our exclusive Lipo-Ignite &reg; injection at a discounted price</li>
        </ul>
        <p class="supervised">Physician-Supervised And Clinically Proven</p>
      </div>
      <div class="program-desc">
        <p>A great option for those looking to dip their toe into the water with medically assisted weight loss. The
          Basic Program provides both a metabolic and energy boost using high-potency B-12 injections.</p>
        <p>Vitamin B-12 is a necessary nutrient for the body to convert carbohydrates into energy, yet approximately 40%
          of Americans may be moderately to severely deficient. Common medications such as metformin, used to treat
          diabetes and Polycystic Ovarian Disease, can also lower the absorption of Vitamin B-12, leading to further
          deficiency.</p>
        <p>An intramuscular injection ensures that a high Vitamin B-12 dosage is delivered more directly into your
          system, unlike pills, which lose their effect and bioavailability when they pass through the stomach.</p>
        <p>Appetite suppressants are given to ensure a comfortable experience. In addition, you'll also receive
          "carb-blocking" mineral supplements to supply your body with the nutrients and support it needs to fully
          benefit from the treatment.</p>
      </div>
    </div>
    <div class="program">
      <div class="program-tile lipo-ignite" id="lipoignite">
        <span class="most-pop">MOST POPULAR</span>
        <p class="program-title">Lipo-Ignite Program</p>
        <span class="lose-amount">LOSE 2 TO 3 LBS / INCHES A WEEK</span>
        <ul>
          <li>Full physician supervision</li>
          <li>Individual consultation</li>
          <li>Nutritional coaching and support</li>
          <li>Lipo-Ignite intramuscular injection, featuring our exclusive formula:</li>
        </ul>
        <span class="view-formula">
          <svg width="17" height="20" viewBox="0 0 17 20" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path
              d="M16.0444 18.7L11.0444 6V2H11.5444C11.8444 2 12.0444 1.8 12.0444 1.5V0.5C12.0444 0.2 11.8444 0 11.5444 0H4.54444C4.24444 0 4.04444 0.2 4.04444 0.5V1.5C4.04444 1.8 4.24444 2 4.54444 2H5.04444V6L0.0444394 18.7C-0.155561 19.3 0.34444 20 1.04444 20H15.0444C15.7444 20 16.2444 19.3 16.0444 18.7ZM4.44444 14L7.04444 6.4V2H9.04444V6.4L11.6444 14H4.44444Z"
              fill="white" />
          </svg>
          <p>View Formula</p>
          <div class="view-formula-desc">
            <p>High-potency Vitamin B1-B6 and Vitamin B-12 - Vitamin B complex that accelerates metabolism and allows
              the body to burn through more calories.</p>
            <p>Methionine, choline, and inositol (MIC) - Lipotropic fat-burning compound used to break down and
              eliminate stubborn body fat, while speeding up the weight loss process.</p>
            <p>Myoden - Powerhouse substance that increases metabolism at the cellular level and provides a rush of
              energy.
            </p>
            <p>Antioxidant and amino acid complex - Compound that cleanses the body of environmental toxins, while
              promoting more energy and a youthful glow.</p>
          </div>
        </span>
        <p class="supervised">Physician-Supervised And Clinically Proven</p>
      </div>
      <div class="program-desc">
        <p>Lipo-Ignite&reg; is our most sought-after treatment because of the custom formulation used in this powerful
          injection. Due to the demand for this program, if Lipo-Ignite&reg; injections are temporarily unavailable, we
          will place you on the waiting list.</p>
        <p>What makes the Lipo-Ignite® injection unique is its combination of lipotropic, vitamin, mineral, antioxidant,
          and amino acid agents. When used together in a high-potency form delivered straight to the muscle, the
          resulting weight loss may be up to half a pound per day. It works by accelerating metabolism to burn through
          fat and calories faster than the body is able to on its own.</p>
        <p>Because the injection is delivered into the muscle and not the vein, it's virtually pain-free (comparable to
          a small pinch). The comprehensive formula in the Lipo-Ignite&reg; injection means that this program is simpler
          and quicker with less medication to keep track of.</p>
        <p>One injection per week, given by our skilled nursing staff is all that's needed (or one injection every 3-4
          days for accelerated results). </p>
      </div>
    </div>
    <div class="program">
      <div class="program-tile elite" id="eliteprogram">
        <p class="program-title">Elite Program</p>
        <span class="lose-amount">LOSE 20 TO 40 LBS IN 6 WEEKS</span>
        <ul>
          <li>Full physician supervision</li>
          <li>Individual consultation</li>
          <li>Nutritional coaching and support</li>
          <li>Our most powerful weight loss injection, featuring medical-grade hCG</li>
          <li>Program orientation and overview</li>
        </ul>
        <p class="supervised">Physician-Supervised And Clinically Proven</p>
      </div>
      <div class="program-desc">
        <p>Our Elite Program leverages medical-grade hCG (human chorionic gonadotropin) to induce a dramatic weight loss
          response. hCG is a naturally-occurring hormone-like substance found in the human placenta. It first emerged as
          a weight loss agent in 1954 when its effects were studied by doctor A. Simeons whose hCG method grew a
          cult-like following.</p>
        <p>hCG exerts its mechanism of action on the hypothalamus, the small area of the brain that controls appetite,
          satiety, and various aspects of metabolism. It works by commanding the hypothalamus to shift the body from
          storage mode into fat-burning mode.</p>
        <p>Today, hCG is used in both men and women alike to accelerate weight loss in people who need to lose 20 pounds
          or more. Unlike estrogen or testosterone, hCG is not a sex hormone, and therefore has no effect on gender
          traits.</p>
        <p>For best results, we'll provide you nutrition counseling to help you reduce calorie intake and enhance the
          effects of the injection. Most patients find reducing their calories to be easy on this program, since hCG
          modifies appetite by helping you to feel full over long periods of time.</p>
        <p>Due to the considerable weight loss potential from hCG injections, this program is a great fit for those
          struggling with mobility issues. No physical activity is required to be successful, only a reduced calorie
          intake made manageable by the injection itself.</p>
      </div>
    </div>
    <div class="program">
      <div class="program-tile crave-x" id="cravexprogram">
        <p class="program-title">Crave-X Program</p>
        <span class="lose-amount">Eat less & reduce stress</span>
        <ul>
          <li>Full physician supervision</li>
          <li>Individual consultation</li>
          <li>Nutritional coaching and support</li>
          <li>Our specially formulated Crave-X capsules, with only the finest pharmaceutical-grade components
          </li>
        </ul>
        <p class="supervised">Physician-Supervised And Clinically Proven</p>
      </div>
      <div class="program-desc">
        <p>Our specially formulated Crave-X capsules are manufactured in an FDA-approved laboratory with only the finest
          pharmaceutical-grade components. It contains 5-HTP, which can be purchased over the counter, but it is not
          effective alone. 5-HTP must be combined with carbidopa, a prescription medication, to be adequately absorbed.
        </p>
        <p>hCG exerts its mechanism of action on the hypothalamus, the small area of the brain that controls appetite,
          satiety, and various aspects of metabolism. It works by commanding the hypothalamus to shift the body from
          storage mode into fat-burning mode.</p>
        <p>5-HTP is a precursor for serotonin, and it occurs naturally in the body. Serotonin helps to improve mood,
          calm anxiety and helps you sleep better at night. Most importantly, when serotonin increases, it makes us feel
          full, and when levels drop, we begin to feel hunger and cravings occur.
          Our unique formula will stop the drop of serotonin levels eliminating stress eating and craving sweets. This
          alone will make it easier for you to lose weight!
        </p>
        <p>Crave-X is not like most appetite suppressants. It can be combined with other medication for increased weight
          loss or used alone to curb your appetite. It's also a great addition to our Lipo-Ignite injections.</p>
        <p>The proprietary blend in Crave-X has been proven effective in numerous clinical studies and has been endorsed
          by many doctors worldwide.
          Crave-X helps to balance your serotonin levels, preventing you from overeating due to stress; it helps you
          stay in control of your appetite and your life. </p>
      </div>
    </div>
  </div>
</div>
