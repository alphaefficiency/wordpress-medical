<div class="container-fluid why-choose">
  <div class="decor decor-l">
    <svg width="254" height="254" viewBox="0 0 254 254" fill="none" xmlns="http://www.w3.org/2000/svg">
      <circle opacity="0.1" cx="127" cy="127" r="113" stroke="white" stroke-width="28"/>
    </svg>      
  </div>
  <div class="container">
    <h2 class="title">Why Choose Medically Assisted Weight Loss?</h2>
    <p>Did you know there are many things beyond your control that prevent you from losing weight? They include a sluggish metabolism, vitamin and mineral deficiencies, and even resistance to insulin. Many people are unaware that they're living with one or more of these conditions. They've simply felt the effects when they struggle to slim down. </p>
    <div class="why-choose-body">
      <div class="item">
        <svg width="45" height="45" viewBox="0 0 45 45" fill="none" xmlns="http://www.w3.org/2000/svg">
          <circle cx="22.5" cy="22.5" r="22.5" fill="#FFD900"/>
          <path d="M22.5 14L19.8726 19.5979L14 20.4904L18.25 24.8496L17.2452 31L22.5 28.0979L27.7548 31L26.75 24.8496L31 20.4964L25.1274 19.5979L22.5 14Z" fill="white"/>
        </svg>
        <p>Medical weight loss helps you take back control by correcting the ailments that force your body to hang onto unwanted fat. Under direct physician supervision, you’ll receive the right medications to drive your body to break down and release its own fat cells. These can include powerful appetite suppressants like phentermine as well as injection treatments like hCG (human chorionic gonadotropin). You do not need to have a medical condition to benefit from medical weight loss. In fact, several of our own physicians and nurses have gone through our programs with results that have lasted to this day.</p>      
      </div>
      <div class="item">
        <svg width="45" height="45" viewBox="0 0 45 45" fill="none" xmlns="http://www.w3.org/2000/svg">
          <circle cx="22.5" cy="22.5" r="22.5" fill="#FFD900"/>
          <path d="M27.6348 24.9735C28.3833 23.7441 28.7792 22.3324 28.7791 20.893C28.7791 16.5326 25.2509 13.0017 20.8896 13C16.5322 13.0013 13 16.5326 13 20.8922C13 25.2478 16.5326 28.7787 20.8922 28.7787C22.3878 28.7787 23.7813 28.3561 24.9722 27.6339L30.3387 33L33 30.3374L27.6348 24.9735ZM20.8913 25.7739C18.1948 25.7674 16.0148 23.5883 16.01 20.8974C16.0118 19.6033 16.5267 18.3627 17.4417 17.4475C18.3567 16.5324 19.5972 16.0173 20.8913 16.0152C23.5865 16.0222 25.7674 18.2013 25.773 20.8974C25.7674 23.5852 23.5857 25.7674 20.8913 25.7739Z" fill="white"/>
        </svg>          
        <p>In addition to our comprehensive medication options, A Better Weigh formulates our own exclusive injection compounds. Patients often come in seeking our popular Lipo-IgniteⓇ injection, a potent lipotropic developed by our medical team to let you safely drop up to 3.5 pounds of body fat per week.</p>      
      </div>
      <div class="item">
        <svg width="45" height="45" viewBox="0 0 45 45" fill="none" xmlns="http://www.w3.org/2000/svg">
          <circle cx="22.5" cy="22.5" r="22.5" fill="#FFD900"/>
          <path d="M22.5 14L19.8726 19.5979L14 20.4904L18.25 24.8496L17.2452 31L22.5 28.0979L27.7548 31L26.75 24.8496L31 20.4964L25.1274 19.5979L22.5 14Z" fill="white"/>
        </svg>   
        <p>You may be wondering whether you’ll need to make extreme lifestyle changes to go through our weight loss programs. The answer is no, medical weight loss allows you to shed significant body fat without any strenuous exercise. So long as you maintain a sensible healthy diet, your body continues to burn its own fat as fuel, resulting in continued weight reduction by the day.
        </p>       
      </div>
      <div class="item">
        <svg width="45" height="45" viewBox="0 0 45 45" fill="none" xmlns="http://www.w3.org/2000/svg">
          <circle cx="22.5" cy="22.5" r="22.5" fill="#FFD900"/>
          <path d="M15.4018 21H29.5982C29.8192 21 30 21.1687 30 21.375V29.5C30 30.3281 29.2801 31 28.3929 31H16.6071C15.7199 31 15 30.3281 15 29.5V21.375C15 21.1687 15.1808 21 15.4018 21ZM30 19.625V18.5C30 17.6719 29.2801 17 28.3929 17H26.7857V15.375C26.7857 15.1688 26.6049 15 26.3839 15H25.0446C24.8237 15 24.6429 15.1688 24.6429 15.375V17H20.3571V15.375C20.3571 15.1688 20.1763 15 19.9554 15H18.6161C18.3951 15 18.2143 15.1688 18.2143 15.375V17H16.6071C15.7199 17 15 17.6719 15 18.5V19.625C15 19.8313 15.1808 20 15.4018 20H29.5982C29.8192 20 30 19.8313 30 19.625Z" fill="white"/>
        </svg>
        <p>Thanks to our proven effective programs, giving yourself the gift of a fit, healthy body no longer needs to be challenging or uncomfortable. If you’d like to learn more about how our programs can work for you, you're welcome to call or email our knowledgeable team. Our friendly staff will guide you to the right resources to help you make the best decision for you.</p>          
      </div>
    </div>
  </div>
  <div class="decor decor-right">
    <svg width="309" height="472" viewBox="0 0 309 472" fill="none" xmlns="http://www.w3.org/2000/svg">
      <circle opacity="0.1" cx="236" cy="236" r="222" stroke="white" stroke-width="28"/>
    </svg>      
  </div>
</div>