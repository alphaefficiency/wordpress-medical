<div class="container-fluid current-specials">
  <div class="container">
    <div class="first">
      <svg class="percentage" width="412" height="358" viewBox="0 0 412 358" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path opacity="0.1" d="M87 193C61.3333 193 40.3333 184.333 24 167C8 149.333 0 125.833 0 96.5C0 67.1666 8 43.8333 24 26.5C40.3333 8.83333 61.3333 0 87 0C112.667 0 133.5 8.83333 149.5 26.5C165.5 43.8333 173.5 67.1666 173.5 96.5C173.5 125.833 165.5 149.333 149.5 167C133.5 184.333 112.667 193 87 193ZM295.5 3.99997H355L116 354H56.5L295.5 3.99997ZM87 152C98.3333 152 107.167 147.333 113.5 138C120.167 128.667 123.5 114.833 123.5 96.5C123.5 78.1666 120.167 64.3333 113.5 55C107.167 45.6666 98.3333 41 87 41C76 41 67.1667 45.8333 60.5 55.5C53.8333 64.8333 50.5 78.5 50.5 96.5C50.5 114.5 53.8333 128.333 60.5 138C67.1667 147.333 76 152 87 152ZM324.5 358C307.833 358 292.833 354.167 279.5 346.5C266.5 338.5 256.333 327.167 249 312.5C241.667 297.833 238 280.833 238 261.5C238 242.167 241.667 225.167 249 210.5C256.333 195.833 266.5 184.667 279.5 177C292.833 169 307.833 165 324.5 165C350.167 165 371 173.833 387 191.5C403.333 208.833 411.5 232.167 411.5 261.5C411.5 290.833 403.333 314.333 387 332C371 349.333 350.167 358 324.5 358ZM324.5 317C335.833 317 344.667 312.333 351 303C357.667 293.333 361 279.5 361 261.5C361 243.5 357.667 229.833 351 220.5C344.667 210.833 335.833 206 324.5 206C313.5 206 304.667 210.667 298 220C291.333 229.333 288 243.167 288 261.5C288 279.833 291.333 293.667 298 303C304.667 312.333 313.5 317 324.5 317Z" fill="white"/>
      </svg>        
      <div class="first-text">
        <h4 class="title">Check Out Our Current Specials</h4>
        <span>DESIGNED TO GIVE YOU THE EDGE YOU NEED</span>
        <p>Save big on A Better Weigh© supplements and health products. Or purchase a gift card for someone you love.</p>
        <a href="#" class="color-btn">Click Here To Save</a>
      </div>
      <div class="img-wrap">
        <img src="@asset('images/home/current-specials.png')" alt="Current Specials" class="img-fluid"/>
      </div>
    </div>
    <div class="questions">
      <h3>Questions? Talk With Our Team</h3>
      <p>Our caring medical team is eager to help anyone looking to better their life. Call or drop us an email to speak with us today.</p>
      <a href="https://betterweigh.kartra.com/page/kAv158" target="_blank" class="color-btn">Get Started Today</a>
    </div>
    {{-- <p class="info-msg">A Better Weigh Medical Weight Loss Center provides you a safe and affordable medical weight loss solution, personalized to your needs and supervised by a physician.</p> --}}
  </div>
</div>