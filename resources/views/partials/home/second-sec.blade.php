<div class="container-fluid second">
  <div class="container">
    <div class="img-wrap">
      <img src="@asset('images/home/second.png')" alt="Explore" class="img-fluid"/>
    </div>
    <div class="second-text">
      <div class="item">
        <div class="item-header">
          <svg width="47" height="45" viewBox="0 0 47 45" fill="none" xmlns="http://www.w3.org/2000/svg">
            <ellipse cx="23.031" cy="22.5" rx="23.031" ry="22.5" fill="url(#paint0_linear)"/>
            <path fill-rule="evenodd" clip-rule="evenodd" d="M13.4668 22.4863C16.0842 19.0906 19.6312 17 23.5426 17C27.454 17 31.0011 19.0906 33.6184 22.4904C33.832 22.7686 33.832 23.1573 33.6184 23.4355C31.0011 26.8312 27.454 28.9219 23.5426 28.9219C19.6312 28.9219 16.0842 26.8312 13.4668 23.4314C13.2532 23.1532 13.2532 22.7645 13.4668 22.4863ZM23.6852 26.1245C25.4046 26.1245 26.7984 24.7627 26.7984 23.083C26.7984 21.4032 25.4046 20.0415 23.6852 20.0415C21.9658 20.0415 20.5719 21.4032 20.5719 23.083C20.5719 24.7627 21.9658 26.1245 23.6852 26.1245Z" fill="white"/>
            <defs>
            <linearGradient id="paint0_linear" x1="0" y1="0" x2="44.9878" y2="46.0494" gradientUnits="userSpaceOnUse">
            <stop stop-color="#FFD900"/>
            <stop offset="1" stop-color="#DD71C6"/>
            </linearGradient>
            </defs>
          </svg>
          <h4>Physician Supervised</h4>            
        </div>
        <p>We offer a simple, more sensible way to lose weight without strenuous activity or an overly restrictive lifestyle. Each physician-supervised program works by accelerating your metabolism to allow your body to finally shed its unwanted pounds.</p>
      </div>
      <div class="item">
        <div class="item-header">
          <svg width="45" height="45" viewBox="0 0 45 45" fill="none" xmlns="http://www.w3.org/2000/svg">
            <circle cx="22.5" cy="22.5" r="22.5" fill="url(#paint0_linear)"/>
            <path d="M28.9096 16L31.1818 18.3537L20.5528 29.3636L14 22.5762L16.2722 20.2225L20.5528 24.6563L28.9096 16Z" fill="white"/>
            <defs>
            <linearGradient id="paint0_linear" x1="0" y1="0" x2="45" y2="45" gradientUnits="userSpaceOnUse">
            <stop stop-color="#FFD900"/>
            <stop offset="1" stop-color="#DD71C6"/>
            </linearGradient>
            </defs>
          </svg>              
          <h4>Proven Success</h4>            
        </div>
        <p>We believe that safe and effective weight loss should be accessible to everyone. That's why our personalized programs are easy to start and surprisingly affordable. Call, book online, or walk into one of our convenient locations today to explore what's possible.</p>
      </div>
      <a href="https://betterweighmedical.com/book-online-now" target="_blank" class="color-btn">Book a Free Consultation</a>
    </div>
    <div class="decor">
      <svg width="301" height="301" viewBox="0 0 301 301" fill="none" xmlns="http://www.w3.org/2000/svg">
        <circle cx="150.5" cy="150.5" r="136.5" stroke="#F9F9F9" stroke-width="28"/>
      </svg>        
    </div>
  </div>
</div>