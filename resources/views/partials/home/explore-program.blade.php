<div class="container-fluid explore">
  <div class="container">
    <div class="explore-text">
      <div class="item">
        <h2 class="title">Explore Our Programs</h2>
        <p>Find the right fit for your weight loss goals. Whether you're looking to lose 10 pounds or over 100, we have an option designed for you.</p>
        <a href="{{get_site_url()}}/ourprograms/" class="color-btn">See Our Programs</a>
      </div>
      <div class="item">
        <h2 class="title">The Complete Guide to Unlocking Weight Loss</h2>
        <p>We've taken all of our greatest weight loss advice and rolled it up into one comprehensive guide that anyone can follow. Download yours free to get expert weight loss tips, healthy grocery checklists, supplement recommendations and more!</p>
        <a href="https://store.betterweighmedical.com/meal-plans/" class="color-btn">Get the Guide Today</a>
      </div>
    </div>
    <div class="img-wrap">
      <img src="@asset('images/home/explore.png')" alt="Explore" class="img-fluid"/>
    </div>
  </div>
  <div class="decor">
    <svg width="310" height="443" viewBox="0 0 310 443" fill="none" xmlns="http://www.w3.org/2000/svg">
      <circle opacity="0.15" cx="88.5" cy="221.5" r="207.5" stroke="white" stroke-width="28"/>
    </svg>      
  </div>
</div>