{{-- 
  gets custom promotions posts and displays them in form on slider with post featured image  
--}}

@php
$args = array(
'post_type' => 'promotions',
'meta_key' => 'activeSliderItem',
'meta_value' => 1,
);

$promotion_slides = get_posts($args);
@endphp

<div class="promotions-slider-wrap">
  <div class="container-fluid promotions-slider">
    @forelse($promotion_slides as $slide)
    <div class="promotion-slide-item">
      <?=get_the_post_thumbnail($slide->ID, 'full')?>
      <div class="mobile-preview">
        <?=$slide->post_content?>
      </div>
      <div class="container post-excerpt">
        <h2>{!! $slide->post_excerpt !!}</h2>
        @if(get_post_meta($slide->ID, 'promotionSubtitle', true))
          <p>{{ get_post_meta($slide->ID, 'promotionSubtitle', true) }}</p>
        @endif
      </div>
    </div>
    @empty
    <div class="promotion-slide-item d-none">
      <img src="@asset('images/home/bwm.png')" alt="mother and child">
    </div>
    @endforelse
  </div>
  <div class="container slider-controls"></div>
</div>
