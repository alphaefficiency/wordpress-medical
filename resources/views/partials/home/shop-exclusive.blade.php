<div class="container-fluid shop-exclusive">
  <div class="img-wrap">
    <img src="@asset('images/home/shop-exclusive.png')" alt="Shop Exclusive" class="img-fluid"/>
  </div>
  <div class="exclusive-text">
    <h2 class="title" style="color: #333;">Shop Our Exclusive Weight Loss Supplements</h2>
    <span>DESIGNED TO GIVE YOU THE EDGE YOU NEED</span>
    <p>A Better Weigh&copy; weight loss supplements are formulated with proprietary blends that supply your body with the nutrients you need to reach and maintain lasting results.</p>
    <a href="https://store.betterweighmedical.com/" target="_blank" class="color-btn">Shop Now</a>
  </div>
</div>