<div class="container-fluid how-it-works">
  <div class="container">
    <div class="works-title">
      <h2 class="title" style="color: #333;">How It Works</h2>
      <p>Getting started is easy! We're here to support you every step of the way.</p>
    </div>
    <div class="works-body">
      <div class="item">
        <svg width="31" height="70" viewBox="0 0 31 70" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M30.9 -4.76837e-06V70H14.7V13H0.7V-4.76837e-06H30.9Z" fill="#73C3F4"/>
        </svg>                  
        <h5>Schedule Your Consultation Online Today</h5>
        <p>Take the first step by getting your consultation scheduled. Our physicians will give you a free medical evaluation and weight loss assessment.</p>
      </div>
      <div class="item">
        <svg width="57" height="72" viewBox="0 0 57 72" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M56.2 58.8V72H3.3V61.5L30.3 36C33.1667 33.2667 35.1 30.9333 36.1 29C37.1 27 37.6 25.0333 37.6 23.1C37.6 20.3 36.6333 18.1667 34.7 16.7C32.8333 15.1667 30.0667 14.4 26.4 14.4C23.3333 14.4 20.5667 15 18.1 16.2C15.6333 17.3333 13.5667 19.0667 11.9 21.4L0.1 13.8C2.83333 9.73333 6.6 6.56666 11.4 4.3C16.2 1.96666 21.7 0.799999 27.9 0.799999C33.1 0.799999 37.6333 1.66666 41.5 3.4C45.4333 5.06666 48.4667 7.46667 50.6 10.6C52.8 13.6667 53.9 17.3 53.9 21.5C53.9 25.3 53.1 28.8667 51.5 32.2C49.9 35.5333 46.8 39.3333 42.2 43.6L26.1 58.8H56.2Z" fill="#73C3F4"/>
        </svg>                   
        <h5>Come In For Your Free Assessment</h5>
        <p>Get a personalized weight loss program, complete with FDA-approved prescription medication, nutrition counseling, and other supportive features like appetite suppressants to ensure your success.</p>
      </div>
      <div class="item">
        <svg width="57" height="72" viewBox="0 0 57 72" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M37.6 28.5C43.7333 29.5 48.4333 31.8 51.7 35.4C54.9667 38.9333 56.6 43.3333 56.6 48.6C56.6 52.6667 55.5333 56.4333 53.4 59.9C51.2667 63.3 48 66.0333 43.6 68.1C39.2667 70.1667 33.9333 71.2 27.6 71.2C22.6667 71.2 17.8 70.5667 13 69.3C8.26667 67.9667 4.23333 66.1 0.9 63.7L7.2 51.3C9.86667 53.3 12.9333 54.8667 16.4 56C19.9333 57.0667 23.5333 57.6 27.2 57.6C31.2667 57.6 34.4667 56.8333 36.8 55.3C39.1333 53.7 40.3 51.4667 40.3 48.6C40.3 42.8667 35.9333 40 27.2 40H19.8V29.3L34.2 13H4.4V-4.76837e-06H53.4V10.5L37.6 28.5Z" fill="#73C3F4"/>
        </svg>                   
        <h5 style="padding-right: 40px">Start Your Perfect Program</h5>
        <p>Come into one of our three Chicago locations (more coming soon!). After your medical evaluation, you'll get our professional recommendation of the best weight loss program for you.</p>
      </div>
    </div>
    <a href="https://betterweighmedical.involve.me/start-your-weight-loss-journey-e4b7d1c2b637" target="_blank" class="color-btn">Let’s Get Started</a>
  </div>
  <div class="decor">
    <svg width="340" height="533" viewBox="0 0 340 533" fill="none" xmlns="http://www.w3.org/2000/svg">
      <circle cx="266.5" cy="266.5" r="252.5" stroke="#F9F9F9" stroke-width="28"/>
    </svg>      
  </div>
</div>