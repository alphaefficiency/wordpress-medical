<div class="container-fluid success-stories">
  <div class="success-content">
    <h3 class="title" style="color: #333;"><span>3,268 </span>Success Stories and Counting</h3>
    <p>See what others are saying about us, & find inspiration and motivation to join them on the path to a better weigh.</p>
    <a href="{{get_site_url()}}/successstories" class="color-btn">See Patient Stories</a>
  </div>
</div>