<div class="container-fluid hero">
  <div class="decor decor-left">
    <svg width="261" height="386" viewBox="0 0 261 386" fill="none" xmlns="http://www.w3.org/2000/svg">
      <circle cx="68" cy="193" r="179" stroke="white" stroke-width="28"/>
    </svg>      
  </div>
  <div class="container">
    <div class="hero-text">
      <h1>The Best Weight Loss Clinic in Chicago Is Here to Help</h1>
      <p>Welcome to A Better Weigh, we're glad you're here! Since we first opened our doors in 2009, our medical weight loss centers have helped thousands achieve the body they're proud of and we can do the same for you. Our three  locations serve <br class="hero-break"> Chicago-Beverly, Tinley Park and Schererville, IN.</p>
      <div class="socials">
        <a href="https://www.instagram.com/betterweighmedical/" target="_blank" rel="noopener noreferrer">
          <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
            <circle cx="15" cy="15" r="15" fill="#DD71C6"/>
            <path d="M14.9981 12.6657C13.7128 12.6657 12.6638 13.7146 12.6638 15C12.6638 16.2854 13.7128 17.3343 14.9981 17.3343C16.2835 17.3343 17.3324 16.2854 17.3324 15C17.3324 13.7146 16.2835 12.6657 14.9981 12.6657ZM21.9992 15C21.9992 14.0333 22.008 13.0754 21.9537 12.1105C21.8994 10.9898 21.6437 9.99512 20.8242 9.17556C20.0029 8.35426 19.01 8.10034 17.8893 8.04605C16.9226 7.99176 15.9648 8.00052 14.9999 8.00052C14.0332 8.00052 13.0754 7.99176 12.1105 8.04605C10.9897 8.10034 9.99508 8.35601 9.17554 9.17556C8.35425 9.99687 8.10034 10.9898 8.04605 12.1105C7.99177 13.0772 8.00052 14.0351 8.00052 15C8.00052 15.9649 7.99177 16.9246 8.04605 17.8895C8.10034 19.0102 8.356 20.0049 9.17554 20.8244C9.99683 21.6457 10.9897 21.8997 12.1105 21.9539C13.0771 22.0082 14.035 21.9995 14.9999 21.9995C15.9665 21.9995 16.9244 22.0082 17.8893 21.9539C19.01 21.8997 20.0047 21.644 20.8242 20.8244C21.6455 20.0031 21.8994 19.0102 21.9537 17.8895C22.0097 16.9246 21.9992 15.9667 21.9992 15ZM14.9981 18.5917C13.0106 18.5917 11.4065 16.9876 11.4065 15C11.4065 13.0124 13.0106 11.4083 14.9981 11.4083C16.9857 11.4083 18.5897 13.0124 18.5897 15C18.5897 16.9876 16.9857 18.5917 14.9981 18.5917ZM18.7368 12.1C18.2728 12.1 17.898 11.7253 17.898 11.2612C17.898 10.7972 18.2728 10.4224 18.7368 10.4224C19.2009 10.4224 19.5756 10.7972 19.5756 11.2612C19.5758 11.3714 19.5542 11.4806 19.5121 11.5824C19.47 11.6842 19.4082 11.7767 19.3303 11.8547C19.2523 11.9326 19.1598 11.9944 19.058 12.0365C18.9562 12.0786 18.847 12.1002 18.7368 12.1Z" fill="white"/>
          </svg>          
        </a>
        <a href="https://twitter.com/mybetterweigh" target="_blank" rel="noopener noreferrer">
          <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
            <circle cx="15" cy="15" r="15" fill="#DD71C6"/>
            <path d="M22 10.3466C21.4794 10.575 20.9246 10.7264 20.3462 10.7999C20.9412 10.4446 21.3954 9.88637 21.6089 9.2135C21.0541 9.54425 20.4416 9.77788 19.7889 9.90825C19.2621 9.34738 18.5114 9 17.6924 9C16.1034 9 14.8241 10.2898 14.8241 11.8709C14.8241 12.0984 14.8434 12.3171 14.8906 12.5254C12.5045 12.409 10.3931 11.2654 8.97475 9.52325C8.72712 9.95288 8.58188 10.4446 8.58188 10.974C8.58188 11.968 9.09375 12.8491 9.85675 13.3593C9.39562 13.3505 8.94325 13.2166 8.56 13.0058C8.56 13.0145 8.56 13.0259 8.56 13.0373C8.56 14.432 9.55487 15.5905 10.8595 15.8574C10.6259 15.9212 10.3712 15.9519 10.107 15.9519C9.92325 15.9519 9.73775 15.9414 9.56362 15.9029C9.9355 17.0395 10.9908 17.8751 12.2455 17.9023C11.269 18.6661 10.0291 19.1264 8.68688 19.1264C8.4515 19.1264 8.22575 19.1159 8 19.087C9.27137 19.9069 10.7781 20.375 12.403 20.375C17.6845 20.375 20.572 16 20.572 12.2077C20.572 12.0809 20.5676 11.9584 20.5615 11.8368C21.1311 11.4325 21.6097 10.9276 22 10.3466Z" fill="white"/>
          </svg>          
        </a>
        <a href="https://www.facebook.com/betterweighmedical/" target="_blank" rel="noopener noreferrer">
          <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
            <circle cx="15" cy="15" r="15" fill="#DD71C6"/>
            <path d="M13.6929 22V15.4308H12V13.0656H13.6929V11.0454C13.6929 9.45785 14.6929 8 16.9971 8C17.93 8 18.6199 8.09177 18.6199 8.09177L18.5655 10.3005C18.5655 10.3005 17.862 10.2935 17.0942 10.2935C16.2633 10.2935 16.1302 10.6864 16.1302 11.3385V13.0656H18.6316L18.5227 15.4308H16.1302V22H13.6929Z" fill="white"/>
          </svg>      
        </a>
      </div>
    </div>
  </div>
  <div class="decor-yellow">
    <div class="img-wrap">
      <img src="@asset('images/home/hero.png')" alt="Hero"/>
      <img src="@asset('images/home/hero-mobile.png')" alt="Hero" class="img-mobile"/>
    </div>
    <div class="decor-right">
      <svg width="222" height="222" viewBox="0 0 222 222" fill="none" xmlns="http://www.w3.org/2000/svg">
        <circle opacity="0.15" cx="111" cy="111" r="97" stroke="white" stroke-width="28"/>
      </svg>        
    </div>
  </div>
</div>