{{--
  Template Name: Blog
--}}

@extends('layouts.app')

@section('content')
  @if(have_posts())
    <div class="container-fluid blog-wrapper">
      <div class="gray"></div>
      @php
       // Get category from slug
if (is_single()) {
$categories = get_the_category();
$category = $categories[0];
} else {
$category = get_category(get_query_var('cat'));
}

$counted = $category->count;
$categoryId = $category->cat_ID;
$slug = $category->slug;

        // Get Posts by page
        $postsPerPage = intval(get_option('posts_per_page'));
        // Total number of pages
        // $totalPages = Posts::categoriesPagination(5, $tagId);
        $totalPages = Posts::getCategoryCount($postsPerPage, $categoryId);

        $res = Posts::getCurrentPage($_GET['pageNumber'], $totalPages);
        $currentPage = $res->currentPage;
        $is404 = $res->is404;

        // Get posts per page
        $posts = Posts::getPostsByCategoryId($postsPerPage, $currentPage, $categoryId);
      @endphp

      {{-- If user requested page that doesn't exist return 404 --}}
      @if ($is404)
        @include('404')
        @php return; @endphp
      @endif
       
      {{-- Display Posts --}}
      <div class="container blog-main">
        <h1 style="display: none;">Blog</h1>
        @component('components.posts.post-list' , ['posts' => $posts]) @endcomponent
        @component('components.blog-aside', ['posts' => $posts]) @endcomponent
      </div>

  </div>

    {{-- Blog Pagination --}}
    <div class="container">
      {{-- @component('components.blog.category-pagination' , ['currentPage' => $currentPage , 'counted' => $totalPages, 'slug' => $slug]) @endcomponent --}}
      @component('components.blog.category-pagination' , ['currentPage' => $currentPage , 'counted' => $totalPages, 'slug' => $slug]) @endcomponent
    </div>
    

    {{-- If there are no posts, display this message --}}
    @else
      <p>@php _e('Sorry, no posts matched your criteria.'); @endphp</p>
  @endif

@endsection

