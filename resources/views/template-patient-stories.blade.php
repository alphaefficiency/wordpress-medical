{{--
Template Name: Patient Stories Template
--}}

@extends('layouts.app')

@section('content')

    @include('components.testimonials')
    <div class="container-fluid patient-stories">
        <div class="testimonial-videos" id="testimonialvideo">
            <div class="container">
                {{-- <div class="testimonial-slider">
                    <div class="slide-item">
                        <iframe src="https://player.vimeo.com/video/519532690" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
                    </div>
                    <div class="slide-item">
                        <iframe src="https://player.vimeo.com/video/519531451" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                    </div>
                    <div class="slide-item">
                        <iframe src="https://player.vimeo.com/video/519530056" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                    </div>
                    <div class="slide-item">
                        <iframe src="https://player.vimeo.com/video/519529489" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                    </div>
                    <div class="slide-item">
                        <iframe src="https://player.vimeo.com/video/519528801" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                    </div>
                    <div class="slide-item">
                        <iframe src="https://player.vimeo.com/video/519528150" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                    </div>
                </div> --}}
                <div id="testimonialsVideosSlider" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                      <div class="carousel-item active">
                        <iframe src="https://player.vimeo.com/video/519532690" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
                      </div>
                      <div class="carousel-item">
                        <iframe src="https://player.vimeo.com/video/519531451" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                      </div>
                      <div class="carousel-item">
                        <iframe src="https://player.vimeo.com/video/519530056" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                      </div>
                      <div class="carousel-item">
                        <iframe src="https://player.vimeo.com/video/519529489" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                      </div>
                      <div class="carousel-item">
                        <iframe src="https://player.vimeo.com/video/519528801" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                      </div>
                      <div class="carousel-item">
                        <iframe src="https://player.vimeo.com/video/519528150" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                      </div>
                    </div>
                  </div>
                <div class="testimonial-arrows">
                    <ul>
                        {{-- <li class="prev carousel-control-prev-icon" href="#testimonialsVideosSlider"><</li>
                        <li class="next" href="#testimonialsVideosSlider">></li> --}}
                        <a class="carousel-control-prev prev" href="#testimonialsVideosSlider" role="button" data-slide="prev">
                            {{-- <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span> --}}
                            <
                        </a>
                        <a class="carousel-control-next" href="#testimonialsVideosSlider" role="button" data-slide="next">
                            {{-- <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span> --}}
                            >
                        </a>
                    </ul>
                </div>
            </div>
        </div>
        
        <div class="women" id="womenreviews">

            <svg class="bubble1" width="254" height="472" viewBox="0 0 254 472" fill="none"
                xmlns="http://www.w3.org/2000/svg">
                <circle opacity="0.15" cx="236" cy="236" r="222" stroke="white" stroke-width="28" />
            </svg>

            <svg class="bubble2" width="154" height="214" viewBox="0 0 154 214" fill="none"
                xmlns="http://www.w3.org/2000/svg">
                <circle opacity="0.15" cx="47" cy="107" r="93" stroke="white" stroke-width="28" />
            </svg>

            <svg class="bubble3" width="252" height="337" viewBox="0 0 252 337" fill="none"
                xmlns="http://www.w3.org/2000/svg">
                <circle opacity="0.15" cx="168.5" cy="168.5" r="154.5" stroke="white" stroke-width="28" />
            </svg>


            <div class="shequita-annete">
                <div class="shequita-before">
                    <div class="before">
                        Before
                    </div>
                </div>
                <div class="purple">
                    <div class="text">
                        <h1>
                            Lost 57 pounds
                        </h1>

                        <h3>
                            Shequita
                        </h3>

                        <p>After I had my third child, I was at the highest weight I’d
                            ever been. And while I was overjoyed to have three beautiful
                            children, I had never been unhappier with myself or the
                            way that I looked. I knew I needed to do something for the
                            sake of my own happiness, but also for that of my family. I
                            really wanted to be the healthiest I could be for my
                            husband and my kids. A Better Weigh has the kinds of
                            programs that any person can be successful on, even a
                            busy working mom like me! Losing the weight has been
                            life-changing for me. I went from a size 18 to a size 10 in four
                            months, and have lost 57 pounds! I have more energy than I
                            ever could have imagined, and I am truly grateful that I
                            found A Better Weigh.</p>
                    </div>
                </div>
                <div class="shequita-after">
                    <div class="after">
                        After
                    </div>
                </div>
            </div>

            <div class="shequita-annete" id="annette">
                <div class="annette-before">
                    <div class="before">
                        Before
                    </div>
                </div>
                <div class="purple">
                    <div class="text">
                        <h1>
                            Lost 64 pounds
                        </h1>

                        <h3>
                            Annete
                        </h3>

                        <p>When I first came to A Better Weigh, I weighed 218 pounds.
                            I chose the Elite HCG Program and made it into the 150’s
                            which I’ve maintained for 6 years and counting! Do I get
                            tired of people telling me “wow you look great!” or the look
                            on someone’s face when they don’t recognize me at first?
                            Never! I am so proud of my new lifestyle and the way I look
                            and feel. When I joined A Better Weigh, I was literally in
                            tears and ready to make a change. I felt defeated by diet
                            programs, books, useless supplements, and things I read in
                            magazines that didn’t work for me. I knew I was going to
                            make this plan work for me. Thanks A Better Weigh.</p>
                    </div>
                </div>
                <div class="annette-after">
                    <div class="after">
                        After
                    </div>
                </div>
            </div>
        </div>

        <div class="gray-bg"></div>
    </div>
    <div class="container-fluid low-bar">
        <div class="container">
            <div class="stories">
                <div class="h1">
                    <h1 class="title">
                        Success <br> Stories
                    </h1>
                </div>
                <div class="p">
                    <p>All of the success stories and testimonials shown are actual patients of A Better Weigh Medical Weight Loss Center. We display them to show what can be accomplished when a person commits to following our program.</p>
                </div>
            </div>
        </div>
    </div>

@endsection
