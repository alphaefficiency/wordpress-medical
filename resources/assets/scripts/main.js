// import external dependencies
import 'jquery';
import 'slick-carousel';

// Import everything from autoload    
import './autoload/**/*'

// import local dependencies
import Router from './util/Router';
import common from './routes/common';
import home from './routes/home';
import subscription from './routes/subscription';
import locations from './routes/locations';
import successstories from './routes/successstories';

/** Populate Router instance with DOM routes */
const routes = new Router({
  // All pages
  common,
  // Home page
  home,
  subscription,
  locations,
  successstories,
});

// Load Events
jQuery(document).ready(() => routes.loadEvents());
