export default {
  init() {
    // JavaScript to be fired on the home page

    function update(){

      // Get current date and time
      var today = new Date();
    
      // Get number of days to Friday
      var dayNum = today.getDay();
      var daysToFri = 14 - (dayNum < 5? dayNum : dayNum - 7);
      
      // Get milliseconds to noon friday
      var fridayNoon = new Date(+today);
      fridayNoon.setDate(fridayNoon.getDate() + daysToFri);
      fridayNoon.setHours(12,0,0,0);
      // Round up ms remaining so seconds remaining matches clock
      var ms = Math.ceil((fridayNoon - today)/1000)*1000;
      var d =  ms / 8.64e7 | 0;
      var h = (ms % 8.64e7) / 3.6e6 | 0;
      var m = (ms % 3.6e6)  / 6e4 | 0;
      var s = (ms % 6e4)    / 1e3 | 0;
      
      // Return remaining 
      // return d + 'd ' + h + 'h ' + m + 'm ' + s + 's';
      document.querySelector('#days').innerText = d;
      document.querySelector('#hours').innerText = h;
      document.querySelector('#minutes').innerText = m;
      document.querySelector('#seconds').innerText = s;
    }
    
    // Run update just after next full second
    function runUpdate() {
      update();
      setTimeout(runUpdate, 1000);
    }
    
     runUpdate();
    
  

    $('.expandable-item').click(function(){
      let item = $('.expandable-item');

      if($(this).hasClass('expandable-active')) {
        $(item).find('.item-expand').slideUp();
        $(item).find('svg').removeClass('active-svg');
        $(item).removeClass('expandable-active');
        // $(item).removeClass('active-svg');
        $(this).find('svg').removeClass('active-svg');
        $(this).find('.item-expand').removeClass('expandable-active');
      } else {
        $(item).find('.item-expand').slideUp();
        $(item).find('svg').removeClass('active-svg');
        $(this).addClass('expandable-active');
        $(this).find('.item-expand').slideDown();
        $(this).find('svg').addClass('active-svg');
      }
    });

    $('.nutrients-wrap .nutrient-item').click(function(){
      let itemID = $(this).data('id');
      let nutrientDescriptions = $('.nutrient-description');
      $('.nutrient-item').removeClass('nutrient-item-active');
      $(this).addClass('nutrient-item-active');

      for(let i = 0; i < nutrientDescriptions.length; i++) {
        let desc = nutrientDescriptions[i];
        
        if(itemID == desc.getAttribute('data-id')) {
          desc.classList.add('nutrient-desc-active');
        } else {
          desc.classList.remove('nutrient-desc-active');
        }
      }
    });

    $('.nutrients-mobile-wrap .nutrient-item .upper').click(function() {
      if($(this).parent().hasClass('active')) {
        $(this).parent().find('.nutrient-description').slideUp();
        $(this).parent().find('.nutrient-description').removeClass('nutrient-desc-active');
        $(this).parent().removeClass('nutrient-item-active');
        $(this).parent().removeClass('active');
        $('.nutrients-mobile-wrap .nutrient-item').removeClass('active');
        $('.nutrients-mobile-wrap .nutrient-item').find('svg').removeClass('svg-active');
      } else {
        $('.nutrients-mobile-wrap .nutrient-item').removeClass('nutrient-item-active');
        $(this).parent().addClass('nutrient-item-active');
        $(this).parent().addClass('active');
        $('.nutrients-mobile-wrap .nutrient-item').find('.nutrient-description').slideUp();
        $(this).next().slideDown();
        $(this).find('svg').addClass('svg-active');
      }
    });

    $('.color-btn').click(function(e) {
      e.preventDefault();
      $('.payment-modal').css('display', 'flex');
      $('.payment-modal').css('overflow', 'scroll');
      $('.payment-modal iframe').css('height', '950px');
      $('body').css('overflow', 'hidden');
    });

    $('.payment-modal .x-btn').click(function(){
      $('.payment-modal').css('display', 'none');
      $('body').css('overflow', 'auto');
    });

    /* eslint-disable */

    // var iframeMouseOver = false;
    // $('#close')
    //   .off("mouseover.iframe").on("mouseover.iframe", function() {
    //     iframeMouseOver = true;
    //   })
    //   .off("mouseout.iframe").on("mouseout.iframe", function() {
    //     iframeMouseOver = false;
    //   });

    // $(window).off("blur.iframe").on("blur.iframe", function() {
    //   if(iframeMouseOver){
    //     $("#close").click();
    //     $('.payment-modal').css('display', 'none');
    //     $('body').css('overflow', 'auto');
    //   }
    // });

  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};