export default {
  init() {
    // JavaScript to be fired on the home page
    $('.promotions-slider').slick({
      infinite: true,
      speed: 500,
      fade: true,
      cssEase: 'linear',
      appendArrows: $('.slider-controls'),
      prevArrow: `<svg width="53" height="53" viewBox="0 0 53 53" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <circle cx="26.5" cy="26.5" r="26.5" transform="rotate(-180 26.5 26.5)" fill="white"/>
                    <path d="M18.3699 26.4998L30.2949 14.5748L31.8 16.0799L21.3801 26.4998L31.8 36.9197L30.2949 38.4248L18.3699 26.4998Z" fill="#DD71C6"/>
                  </svg>`,
      nextArrow: `<svg width="53" height="53" viewBox="0 0 53 53" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <circle cx="26.5" cy="26.5" r="26.5" fill="white"/>
                    <path d="M34.6301 26.5002L22.7051 38.4252L21.2 36.9201L31.6199 26.5002L21.2 16.0803L22.7051 14.5752L34.6301 26.5002Z" fill="#DD71C6"/>
                  </svg>`,
      autoplay: true,
      autoplaySpeed: 6000,
    });
      
  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};
