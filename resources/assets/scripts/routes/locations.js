export default {
  init() {
    $(document).ready(() => {
 /*eslint-disable*/
    // JavaScript to be fired on all pages

    const directionsService = new google.maps.DirectionsService();
    const directionsDisplay = new google.maps.DirectionsRenderer();


    let locations = $('.store-location-single .card-title');
    $(locations).on('click', function() {
      if($(this).hasClass('active-location')) {
        // locations.next().slideUp();
        // locations.removeClass('active-location');
        // locations.css('background', '#fff');
        // locations.find('p').removeClass('active-p');
        // locations.find('svg').find('path').css('fill', '#000');
        // locations.find('svg').removeClass('active-svg');
      } else {
        // locations.removeClass('active-location');
        // locations.next().slideUp();
        // locations.css('background', '#fff');
        // locations.find('p').removeClass('active-p');
        // locations.find('svg').find('path').css('fill', '#000');
        // locations.find('svg').removeClass('active-svg');
        // $(this).addClass('active-location');
        // $(this).next().slideDown();
        // $(this).css('background', '#DD71C6');
        // $(this).find('p').addClass('active-p');
        // $(this).find('svg').find('path').css('fill', '#fff');
        // $(this).find('svg').addClass('active-svg');
      }
      $('#beverly').click(function(e) {
        e.preventDefault();
        const map = new google.maps.Map(document.getElementById('map'), {
          zoom: 15,
          center: { lat: 41.72090171983067, lng: -87.6710401989152},
        });
  
        const marker = new google.maps.Marker({
          position: { lat: 41.72, lng: -87.671 },
          map: map,
        });
  
        var destination = new google.maps.LatLng(41.72, -87.671);
        directionsDisplay.setMap(map);
  
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(calculateRoute);
        }
  
        function calculateRoute(pos){
  
          let location= {
            lat: parseFloat( pos.coords.latitude ),
            lng: parseFloat( pos.coords.longitude )
          }
  
          var request = {
            origin: location,
            destination: destination,
            travelMode: 'DRIVING'
          };
  
          directionsService.route(request, function(result, status){
  
            if(status == "OK"){
              directionsDisplay.setDirections(result);
            }
          });
        }
      });
  
      $('#edgewater').click(function(e) {
        e.preventDefault();
        const map = new google.maps.Map(document.getElementById('map'), {
          zoom: 15,
          center: { lat: 41.97956596038968, lng: -87.66166876323533},  
        });
  
        const marker = new google.maps.Marker({
          position: { lat: 41.979101979214256,  lng: -87.65484810211629 },
          map: map,
        });
  
        var destination = new google.maps.LatLng(41.72, -87.671);
        directionsDisplay.setMap(map);
  
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(calculateRoute);
        }
  
        function calculateRoute(pos){
  
          let location= {
            lat: parseFloat( pos.coords.latitude ),
            lng: parseFloat( pos.coords.longitude )
          }
  
          var request = {
            origin: location,
            destination: destination,
            travelMode: 'DRIVING'
          };
  
          directionsService.route(request, function(result, status){
  
            if(status == "OK"){
              directionsDisplay.setDirections(result);
            }
          });
        }
        
      });
  
      $('#tinley').click(function(e) {
        e.preventDefault();
        const map = new google.maps.Map(document.getElementById('map'), {
          zoom: 15,
          center: { lat: 41.580718215742415, lng: -87.78908904621996}, 
        });
  
        const marker = new google.maps.Marker({
          position: { lat: 41.58073276178616, lng: -87.78496380883662 },
          map: map,
        });
  
        var destination = new google.maps.LatLng(41.58, -87.78);
        directionsDisplay.setMap(map);
  
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(calculateRoute);
        }
  
        function calculateRoute(pos){
  
          let location= {
            lat: parseFloat( pos.coords.latitude ),
            lng: parseFloat( pos.coords.longitude )
          }
  
          var request = {
            origin: location,
            destination: destination,
            travelMode: 'DRIVING'
          };
  
          directionsService.route(request, function(result, status){
  
            if(status == "OK"){
              directionsDisplay.setDirections(result);
            }
          });
        }
      });

      $('#Schererville').click(function(e) {
        e.preventDefault();
        const map = new google.maps.Map(document.getElementById('map'), {
          zoom: 15,
          center: { lat: 41.48184408472896, lng: -87.45682451236367}, 
        });
  
        const marker = new google.maps.Marker({
          position: { lat: 41.48184408472896, lng: -87.45682451236367 },
          map: map,
        });
  
        var destination = new google.maps.LatLng(41.48, -87.45);
        directionsDisplay.setMap(map);
  
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(calculateRoute);
        }
  
        function calculateRoute(pos){
  
          let location= {
            lat: parseFloat( pos.coords.latitude ),
            lng: parseFloat( pos.coords.longitude )
          }
  
          var request = {
            origin: location,
            destination: destination,
            travelMode: 'DRIVING'
          };
  
          directionsService.route(request, function(result, status){
  
            if(status == "OK"){
              directionsDisplay.setDirections(result);
            }
          });
        }
      });

    });
    });
   

 
    
  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
