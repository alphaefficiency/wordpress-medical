export default {
  init() {
    // JavaScript to be fired on all pages
    $('.burger-btn').click(function() {
      $('.burger-menu').toggleClass('burger-active');
    });
  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
