export default {
  init() {
    // JavaScript to be fired on all pages
    /* eslint-disable */
    $('.testimonial-slider').slick({
      centerMode: true,
      // centerPadding: '60px',
      slidesToShow: 2,
      prevArrow: $('.prev'),
      nextArrow: $('.next'),
      variableWidth: true,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            centerMode: true,
            centerPadding: '40px',
            slidesToShow: 2
          }
        },
        {
          breakpoint: 720,
          settings: {
            arrows: false,
            centerMode: true,
            centerPadding: '40px',
            slidesToShow: 1
          }
        }
      ]
    });

    // $('.testimonial-arrows ul a').click(function(){      
    //   // $('iframe').attr('src', $('iframe').attr('src'));
    //   // iframe.postMessage('{"method":"pause"}', '*');
    //   $("iframe").each(function() { 
    //     var src= $(this).attr('src');
    //     $(this).attr('src',src);  
    //   });
    // });
    
  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
